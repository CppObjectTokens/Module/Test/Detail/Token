/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Spec/Editor.h>
#include <Test/Token/Bind.h>
#include <Test/Token/Basis.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>

namespace Test
{

namespace Conform
{

namespace Internal
{

template <class Spec, class = void>
struct OwnerSpec
{
    using Type = Test::Spec::ReplaceOwnership<Spec,
                                              Test::Ownership::Shared>;
};

template <class Element, class Basis, class Multiplicity>
struct OwnerSpec<Test::Spec::Generic<Element,
                                     Basis,
                                     Test::Ownership::Unique,
                                     Multiplicity>>
{
    using Type = Test::Spec::Generic<Element,
                                     Basis,
                                     Test::Ownership::Unique,
                                     Multiplicity>;
};

template <class Spec, class = void>
struct ProxySpec
{
    using Type = Test::Spec::ReplaceOwnership<
        Test::Spec::ReplaceMultiplicity<Spec, Test::Multiplicity::Optional>,
        Test::Ownership::Unified>;
};

} // namespace Internal

template <class Spec>
using OwnerSpec = typename Internal::OwnerSpec<Spec>::Type;

template <class Spec>
using ProxySpec = typename Internal::ProxySpec<Spec>::Type;

} // namespace Conform

} // namespace Test
