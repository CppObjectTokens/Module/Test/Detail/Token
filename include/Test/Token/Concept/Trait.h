/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct ConceptTrait
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class = void>
struct TraitIsValid
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;

        INFO("Concept trait is valid"
             "\nof " + Utility::typeName<Subject>())
        {
            REQUIRE(std::is_same_v<Bind::Trait::Element<Subject>,
                                   typename SubjectSpec::Element>);
            REQUIRE(std::is_same_v<Bind::Trait::Basis<Subject>,
                                   Bind::Gateway::Outer<typename SubjectSpec::Basis>>);
            REQUIRE(std::is_same_v<Bind::Trait::Ownership<Subject>,
                                   Bind::Gateway::Outer<typename SubjectSpec::Ownership>>);
            REQUIRE(std::is_same_v<Bind::Trait::Multiplicity<Subject>,
                                   Bind::Gateway::Outer<typename SubjectSpec::Multiplicity>>);

            REQUIRE(Bind::Trait::Lifetime::Role<Subject>
                    == Bind::Gateway::OuterValue(Test::Trait::Lifetime<Subject>::Role));

            REQUIRE(Bind::Trait::IsToken<Subject> == Spec::IsToken<SubjectSpec>);
            REQUIRE(Bind::Trait::IsOwner<Subject> == Spec::IsOwner<SubjectSpec>);
            REQUIRE(Bind::Trait::IsObserver<Subject> == Spec::IsObserver<SubjectSpec>);
            REQUIRE(Bind::Trait::IsOptional<Subject> == Spec::IsOptional<SubjectSpec>);
            REQUIRE(Bind::Trait::IsSingle<Subject> == Spec::IsSingle<SubjectSpec>);

            REQUIRE(Bind::Trait::IsTrackable<Subject> == Spec::IsTrackable<SubjectSpec>);
            REQUIRE(Bind::Trait::IsDereferenceable<Subject> == Spec::IsDereferenceable<SubjectSpec>);
            REQUIRE(Bind::Trait::IsDirectAccessible<Subject> == Spec::IsDirectAccessible<SubjectSpec>);
            REQUIRE(Bind::Trait::IsProxyAccessible<Subject> == Spec::IsProxyAccessible<SubjectSpec>);
        }
    }
};

template <class SubjectSpec>
void traitIsValid()
{
    using namespace Combo::Unary;
    Apply<TraitIsValid,
          ConstSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct ConceptTrait<SubjectSpec>
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;
        using ConcreteSpec    = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        traitIsValid<SubjectSpec>();

        if constexpr (IsDerived<ConcreteElement, Element>)
            traitIsValid<ConcreteSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
