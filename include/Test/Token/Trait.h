/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>

namespace Test
{

namespace Trait
{

// T - Token

template <class T>
using Spec = Bind::Rule::Spec<T>;

template <class T>
using Basis = typename Spec<T>::Basis;

template <class T>
using Ownership = typename Spec<T>::Ownership;

template <class T>
using Multiplicity = typename Spec<T>::Multiplicity;

template <class T>
using Element = typename Spec<T>::Element;

template <class T>
using Lifetime = Bind::Rule::Lifetime<Spec<T>>;

template <class T>
inline constexpr bool IsLockable = Bind::Rule::IsLockable<Spec<T>>;

template <class T>
inline constexpr bool IsExpirable = Bind::Rule::IsExpirable<Spec<T>>;

template <class T>
inline constexpr bool IsDereferenceable = Bind::Rule::IsDereferenceable<Spec<T>>;

template <class T>
inline constexpr bool IsDirectAccessible = Bind::Rule::IsDirectAccessible<Spec<T>>;

template <class T>
inline constexpr bool IsProxyAccessible = Bind::Rule::IsProxyAccessible<Spec<T>>;

template <class T>
inline constexpr bool IsPointerConstructible = Bind::Rule::IsPointerConstructible<Spec<T>>;

template <class T>
inline constexpr bool IsItselfConstructible = Bind::Rule::IsItselfConstructible<Spec<T>>;

template <class T>
inline constexpr bool IsItselfTypeConstructible = Bind::Rule::IsItselfTypeConstructible<Spec<T>>;

template <class T>
inline constexpr bool IsOwnerBased = Bind::Rule::IsOwnerBased<Spec<T>>;

template <class T, class Feature>
inline constexpr bool HasFeature = Bind::Rule::HasFeature<Spec<T>, Feature>;

} // namespace Trait

} // namespace Test
