/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Bind.h>
#include <Test/Token/Basis.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>
#include <Test/Token/Lifetime.h>
#include <Test/Token/Feature.h>

namespace Test
{

namespace Spec
{

// S - Spec

// ----- Basis -----------------------------------------------------------------
template <class S>
inline constexpr bool IsUnknown = Basis::IsUnknown<typename S::Basis>;

template <class S>
inline constexpr bool IsStdSmart = Basis::IsStdSmart<typename S::Basis>;
// ----- Basis -----------------------------------------------------------------

// ----- Ownership -------------------------------------------------------------
template <class S>
inline constexpr bool IsUnified = Ownership::IsUnified<typename S::Ownership>;

template <class S>
inline constexpr bool IsUnique = Ownership::IsUnique<typename S::Ownership>;

template <class S>
inline constexpr bool IsShared = Ownership::IsShared<typename S::Ownership>;

template <class S>
inline constexpr bool IsWeak = Ownership::IsWeak<typename S::Ownership>;
// ----- Ownership -------------------------------------------------------------

// ----- Multiplicity ----------------------------------------------------------
template <class S>
inline constexpr bool IsMultiplicityUnknown = Multiplicity::IsUnknown<typename S::Multiplicity>;

template <class S>
inline constexpr bool IsOptional = Multiplicity::IsOptional<typename S::Multiplicity>;

template <class S>
inline constexpr bool IsSingle = Multiplicity::IsSingle<typename S::Multiplicity>;
// ----- Multiplicity ----------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <class S>
inline constexpr bool IsToken = Lifetime::IsToken<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsOwner = Lifetime::IsOwner<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsObserver = Lifetime::IsObserver<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsTrackable = Lifetime::IsTrackable<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsLockable = Bind::Rule::IsLockable<S>;

template <class S>
inline constexpr bool IsExpirable = Bind::Rule::IsExpirable<S>;

template <class S>
inline constexpr bool IsUntrackableObserver = Lifetime::IsUntrackableObserver<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsTrackableObserver = Lifetime::IsTrackableObserver<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsUntrackableOwner = Lifetime::IsUntrackableOwner<Bind::Rule::Lifetime<S>>;

template <class S>
inline constexpr bool IsTrackableOwner = Lifetime::IsTrackableOwner<Bind::Rule::Lifetime<S>>;
// ----- Lifetime --------------------------------------------------------------

// ----- Access ----------------------------------------------------------------
template <class S>
inline constexpr bool IsDereferenceable = Bind::Rule::IsDereferenceable<S>;

template <class S>
inline constexpr bool IsDirectAccessible = Bind::Rule::IsDirectAccessible<S>;

template <class S>
inline constexpr bool IsProxyAccessible = Bind::Rule::IsProxyAccessible<S>;
// ----- Access ----------------------------------------------------------------

// ----- Construction ----------------------------------------------------------
template <class S>
inline constexpr bool IsPointerConstructible = Bind::Rule::IsPointerConstructible<S>;

template <class S>
inline constexpr bool IsItselfConstructible = Bind::Rule::IsItselfConstructible<S>;

template <class S>
inline constexpr bool IsItselfTypeConstructible = Bind::Rule::IsItselfTypeConstructible<S>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsCopyable = Bind::Rule::CanCopy<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMovable = Bind::Rule::CanMove<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsExplicitCopy = Bind::Rule::IsExplicitCopy<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsExplicitMove = Bind::Rule::IsExplicitMove<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveEmpty = Bind::Rule::IsMoveEmpty<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveUnspecified = Bind::Rule::IsMoveUnspecified<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveRelocateObject = Bind::Rule::IsMoveRelocateObject<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveInvalidateObservers = Bind::Rule::IsMoveInvalidateObservers<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsTolerateMovedFrom =
    IsMoveEmpty<SourceSpec, SourceSpec>
    && !IsMoveRelocateObject<SourceSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsElementCopyable =
    std::is_convertible_v<typename SourceSpec::Element*,
                          typename SubjectSpec::Element*>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsElementMoveConstructible =
    std::is_convertible_v<typename SourceSpec::Element*,
                          typename SubjectSpec::Element*>
    && (  !IsMoveRelocateObject<SubjectSpec, SourceSpec>
       || std::is_polymorphic_v<typename SubjectSpec::Element>
       || std::is_constructible_v<typename SubjectSpec::Element,
                                  typename SourceSpec::Element &&>);

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsElementMoveAssignable =
    std::is_convertible_v<typename SourceSpec::Element*,
                          typename SubjectSpec::Element*>
    && (  !IsMoveRelocateObject<SubjectSpec, SourceSpec>
       || std::is_polymorphic_v<typename SubjectSpec::Element>
       || (IsMoveRelocateObject<SubjectSpec, SubjectSpec>
           ? std::is_assignable_v<typename SubjectSpec::Element&,
                                  typename SourceSpec::Element &&>
           : std::is_constructible_v<typename SubjectSpec::Element,
                                     typename SourceSpec::Element &&>));

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsCopyNoexcept = Bind::Rule::IsCopyNoexcept<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveNoexcept = Bind::Rule::IsMoveNoexcept<SubjectSpec, SourceSpec>;
// ----- Construction ----------------------------------------------------------

// ----- OwnerBased ------------------------------------------------------------
template <class S>
inline constexpr bool IsOwnerBased = Bind::Rule::IsOwnerBased<S>;
// ----- OwnerBased ------------------------------------------------------------

// ----- Feature ---------------------------------------------------------------
template <class S, class Feature>
inline constexpr bool HasFeature =
    Bind::Rule::HasFeature<typename S::Basis, typename S::Ownership, typename S::Multiplicity, Feature>;
// ----- Feature ---------------------------------------------------------------

// ----- Compound --------------------------------------------------------------
template <class S>
inline constexpr bool IsNotEmpty = IsOwner<S>&& IsSingle<S>;

template <class S>
inline constexpr bool IsMaybeEmpty = !IsNotEmpty<S>;

template <class S>
inline constexpr bool IsSingleUntrackableObserver = IsSingle<S>&& IsUntrackableObserver<S>;
// ----- Compound --------------------------------------------------------------

} // namespace Spec

} // namespace Test
