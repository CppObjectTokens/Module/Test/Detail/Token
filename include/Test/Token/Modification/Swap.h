/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct SwapModification
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class = void>
struct SwapIsValid {};

template <class SubjectSpec>
struct SwapIsValid<SubjectSpec>
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        using std::swap;

        INFO("Swap is valid"
             "\nswap(" + Utility::typeName<Subject>()
             + ",\n     " + Utility::typeName<Subject>() + ")")
        {
            REQUIRE(std::is_swappable_v<Subject>);

            auto literal = literalOf<ConcreteElement>();

            if constexpr (Spec::IsOptional<SubjectSpec>)
            {
                // INFO("Swap empty vs empty")
                {
                    Subject subject;
                    Subject source;

                    swap(subject, source);
                    REQUIRE(isEmpty(subject));
                    REQUIRE(isEmpty(source));

                    swap(subject, source);
                    REQUIRE(isEmpty(subject));
                    REQUIRE(isEmpty(source));
                }
            }

            // INFO("Swap base vs base")
            {
                Owner    subject_owner{makeOwner<Owner, ConcreteElement>(literal)};
                Element* subject_raw = toAddress(subject_owner);
                Owner    source_owner{makeOwner<Owner, ConcreteElement>(literal)};
                Element* source_raw = toAddress(source_owner);

                auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                auto&& source  = forwardOwner<SubjectSpec>(source_owner);

                swap(subject, source);
                O_T_CHECK_NOEXCEPT(swap(subject, source));
                REQUIRE(isObserverIntegrity<SubjectSpec, SubjectSpec>(subject, source_raw));
                REQUIRE(isObserverIntegrity<SubjectSpec, SubjectSpec>(source, subject_raw));

                swap(subject, source);
                REQUIRE(isPointTo(subject, subject_raw));
                REQUIRE(isPointTo(source, source_raw));

                cleanup<OwnerSpec, SubjectSpec>(subject_raw);
                cleanup<OwnerSpec, SubjectSpec>(source_raw);
            }

            if constexpr (Spec::IsMoveEmpty<SubjectSpec, SubjectSpec>)
            {
                // INFO("Swap base vs moved-from")
                {
                    Owner    subject_owner{makeOwner<Owner, ConcreteElement>(literal)};
                    Element* subject_raw = toAddress(subject_owner);
                    Owner    source_owner{makeOwner<Owner, ConcreteElement>(literal)};
                    Element* source_raw = toAddress(source_owner);

                    auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                    auto&& source  = forwardOwner<SubjectSpec>(source_owner);

                    utilize(std::move(source));

                #ifdef TOKEN_TEST_ASSERT
                    if constexpr (Spec::IsSingle<SubjectSpec>)
                    {
                        O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                            swap(subject, source),
                            "swap_with_valued(other)");
                    }
                    else
                #endif
                    {
                        swap(subject, source);
                        REQUIRE(isEmpty(subject));
                        REQUIRE(isObserverIntegrity<SubjectSpec, SubjectSpec>(source, subject_raw));

                        swap(subject, source);
                        REQUIRE(isPointTo(subject, subject_raw));
                        REQUIRE(isMoved(source));
                    }

                    cleanup<OwnerSpec, SubjectSpec>(subject_raw);
                    cleanup<OwnerSpec, SubjectSpec>(source_raw);
                }

                // INFO("Swap moved-from vs base")
                {
                    Owner    subject_owner{makeOwner<Owner, ConcreteElement>(literal)};
                    Element* subject_raw = toAddress(subject_owner);
                    Owner    source_owner{makeOwner<Owner, ConcreteElement>(literal)};
                    Element* source_raw = toAddress(source_owner);

                    auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                    auto&& source  = forwardOwner<SubjectSpec>(source_owner);

                    utilize(std::move(subject));

                #ifdef TOKEN_TEST_ASSERT
                    if constexpr (Spec::IsSingle<SubjectSpec>)
                    {
                        O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                            swap(subject, source),
                            "swap_with_valued(other)");
                    }
                    else
                #endif
                    {
                        swap(subject, source);
                        REQUIRE(isObserverIntegrity<SubjectSpec, SubjectSpec>(subject, source_raw));
                        REQUIRE(isEmpty(source));

                        swap(source, subject);
                        REQUIRE(isEmpty(subject));
                        REQUIRE(isPointTo(source, source_raw));
                    }

                    cleanup<OwnerSpec, SubjectSpec>(subject_raw);
                    cleanup<OwnerSpec, SubjectSpec>(source_raw);
                }
            }

            if constexpr (Spec::IsCopyable<SubjectSpec, SubjectSpec>)
            {
                // INFO("Swap same value")
                {
                    Element* raw = new ConcreteElement(literal);
                    Owner    owner{raw};
                    Subject  subject = forwardOwner<SubjectSpec>(owner);
                    Subject  source{subject};

                    swap(subject, source);
                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(source, raw));

                    swap(subject, source);
                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(source, raw));

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }
            }

            if constexpr (IsDerived<ConcreteElement, Element>)
            {
                // INFO("Swap derived vs derived " + Utility::typeName<ConcreteSubject>())
                {
                    REQUIRE(std::is_swappable_v<ConcreteSubject>);

                    ConcreteOwner    subject_owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    ConcreteElement* subject_raw = toAddress(subject_owner);
                    ConcreteOwner    source_owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    ConcreteElement* source_raw = toAddress(source_owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(subject_owner);
                    auto&& source  = forwardOwner<ConcreteSubjectSpec>(source_owner);

                    swap(subject, source);
                    REQUIRE(isPointTo(subject, source_raw));
                    REQUIRE(isPointTo(source, subject_raw));

                    swap(subject, source);
                    REQUIRE(isPointTo(subject, subject_raw));
                    REQUIRE(isPointTo(source, source_raw));

                    cleanup<OwnerSpec, SubjectSpec>(subject_raw);
                    cleanup<OwnerSpec, SubjectSpec>(source_raw);
                }
            }
        }
    }
};

template <class SubjectSpec, class = void>
struct SwapBaseDerivedIsInvalid {};

template <class SubjectSpec>
struct SwapBaseDerivedIsInvalid<SubjectSpec>
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;

        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;

        using std::swap;

        if constexpr (IsDerived<ConcreteElement, Element>)
        {
            INFO("Swap is invalid of base with derived"
                 "\nswap(" + Utility::typeName<Subject>()
                 + ",\n     " + Utility::typeName<ConcreteSubject>() + ")")
            {
                REQUIRE(!std::is_swappable_with_v<Subject, ConcreteSubject>);

            #ifdef ENABLE_COMPILE_ERROR
                using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;
                using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;
                using SubjectOwner        = Instance<OwnerSpec>;
                using ConcreteOwner       = Instance<Spec::ReplaceElement<OwnerSpec, ConcreteElement>>;

                auto literal = literalOf<ConcreteElement>();

                SubjectOwner  subject_owner{makeOwner<SubjectOwner, ConcreteElement>(literal)};
                ConcreteOwner source_owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                auto&&        subject = forwardOwner<SubjectSpec>(subject_owner);
                auto&&        source  = forwardOwner<ConcreteSubjectSpec>(source_owner);

                swap(subject, source);
            #endif
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec, class = void>
struct SwapIsInvalid {};

template <class SubjectSpec, class SourceSpec>
struct SwapIsInvalid<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;
        using Source  = Instance<SourceSpec>;

        using std::swap;

        INFO("Swap is invalid"
             "\nswap(" + Utility::typeName<Subject>()
             + ",\n     " + Utility::typeName<Source>() + ")")
        {
            REQUIRE(!std::is_swappable_with_v<Subject, Source>);

        #ifdef ENABLE_COMPILE_ERROR
            using SubjectElement        = typename SubjectSpec::Element;
            using ConcreteElement       = Conform::ConcreteOf<SubjectElement>;
            using SourceElement         = typename SourceSpec::Element;
            using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;
            using SubjectOwner = Instance<Conform::OwnerSpec<SubjectSpec>>;
            using SourceOwner  = Instance<Conform::OwnerSpec<SourceSpec>>;

            auto subject_literal = literalOf<ConcreteElement>();
            auto source_literal  = literalOf<ConcreteSourceElement>();

            SubjectOwner subject_owner{makeOwner<SubjectOwner, ConcreteElement>(subject_literal)};
            SourceOwner  source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(source_literal)};
            auto&&       subject = forwardOwner<SubjectSpec>(subject_owner);
            auto&&       source  = forwardOwner<SourceSpec>(source_owner);

            swap(subject, source);
        #endif
        }
    }
};

template <class SubjectSpec, class = void>
struct SwapIsValidElement
{
    void operator()()
    {
        if constexpr (Spec::IsElementMoveAssignable<SubjectSpec, SubjectSpec>)
        {
            SwapIsValid<SubjectSpec>()();
            SwapBaseDerivedIsInvalid<SubjectSpec>()();
        }
        else
        {
            SwapIsInvalid<SubjectSpec, SubjectSpec>()();
        }
    }
};

template <class SubjectSpec>
void swapIsValid()
{
    using namespace Combo::Unary;
    Apply<SwapIsValidElement,
          ConstSet<SubjectSpec>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct SwapIncompatible {};

template <class SubjectSpec, class SourceSpec>
struct SwapIncompatible<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;
        using Source  = Instance<SourceSpec>;

        using std::swap;

        INFO("Swap is incompatible"
             "\nswap(" + Utility::typeName<Subject>()
             + ",\n     " + Utility::typeName<Source>() + ")")
        {
            REQUIRE(!std::is_swappable_with_v<Subject, Source>);

        #ifdef ENABLE_COMPILE_ERROR
            using SubjectElement        = typename SubjectSpec::Element;
            using ConcreteElement       = Conform::ConcreteOf<SubjectElement>;
            using SourceElement         = typename SourceSpec::Element;
            using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;
            using SubjectOwner = Instance<Conform::OwnerSpec<SubjectSpec>>;
            using SourceOwner  = Instance<Conform::OwnerSpec<SourceSpec>>;

            auto subject_literal = literalOf<ConcreteElement>();
            auto source_literal  = literalOf<ConcreteSourceElement>();

            SubjectOwner subject_owner{makeOwner<SubjectOwner, ConcreteElement>(subject_literal)};
            SourceOwner  source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(source_literal)};
            auto&&       subject = forwardOwner<SubjectSpec>(subject_owner);
            auto&&       source  = forwardOwner<SourceSpec>(source_owner);

            swap(subject, source);
        #endif
        }
    }
};

template <class SubjectSpec>
void swapIncompatible()
{
    using namespace Combo::Binary;
    Apply<SwapIncompatible, Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

template <class SubjectSpec>
void swapConstIncorrect()
{
    using Element          = typename SubjectSpec::Element;
    using SpecPair         = std::pair<SubjectSpec, SubjectSpec>;
    using ConstMutableSpec =
        std::pair<Spec::ReplaceElement<SubjectSpec, const Element>, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<SwapIsInvalid,
          ConstMutableSpec>()();
    Apply<SwapIsInvalid,
          ConstIncorrectSet<SpecPair>>()();
}

} // namespace

template <class SubjectSpec>
struct SwapModification<SubjectSpec>
{
    void operator()()
    {
        swapIsValid<SubjectSpec>();
        swapIncompatible<SubjectSpec>();
        swapConstIncorrect<SubjectSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
