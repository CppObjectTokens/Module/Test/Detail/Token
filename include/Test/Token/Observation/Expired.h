/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct ExpiredObservation
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

template <class SubjectSpec>
struct ExpiredObservation<SubjectSpec,
                          std::enable_if_t<Spec::IsExpirable<SubjectSpec>>>
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;

        using Subject = Instance<SubjectSpec>;
        using Owner   = Instance<OwnerSpec>;

        INFO("Expired observation is valid"
             "\nof " + Utility::typeName<Subject>())
        {
            auto literal = literalOf<ConcreteElement>();

            if constexpr (Spec::IsOptional<SubjectSpec>)
            {
                // INFO("of empty")
                {
                    Subject subject;
                    REQUIRE(subject.expired());
                }
            }

            // INFO("of value")
            {
                Owner  owner{makeOwner<Owner, ConcreteElement>(literal)};
                auto&& subject = forwardOwner<SubjectSpec>(owner);

                O_T_CHECK_NOEXCEPT(subject.expired());
                REQUIRE(!subject.expired());
                utilize(std::move(subject));
                REQUIRE(subject.expired());
            }

            // INFO("of deleted owner")
            {
                Owner  owner{makeOwner<Owner, ConcreteElement>(literal)};
                auto&& subject = forwardOwner<SubjectSpec>(owner);

                REQUIRE(!subject.expired());
                utilize(std::move(owner));
                REQUIRE(subject.expired());
            }
        }
    }
};

template <class SubjectSpec>
struct ExpiredObservation<SubjectSpec,
                          std::enable_if_t<!Spec::IsExpirable<SubjectSpec>>>
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using Subject = Instance<SubjectSpec>;

        INFO("Expired observation is invalid"
             "\nof " + Utility::typeName<Subject>())
        {
            Subject subject;
            bool    is_expired = subject.expired();
            REQUIRE(is_expired);
        }
    #endif
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
