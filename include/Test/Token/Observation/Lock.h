/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct LockObservation
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec>
using LockerSpec = Spec::ReplaceOwnership<Spec::ReplaceMultiplicity<SubjectSpec, Multiplicity::Optional>,
                                          std::conditional_t<Spec::IsStdSmart<SubjectSpec>,
                                                             Ownership::Shared,
                                                             Ownership::Unified>>;

template <class SubjectSpec, class SourceSpec, class = void>
struct LockCompatible
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using LockerSpec = Test::LockerSpec<SubjectSpec>;
        using OwnerSpec  = Conform::OwnerSpec<Spec::ReplaceMultiplicity<SourceSpec, Multiplicity::Optional>>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;
        using ConcreteLockerSpec  = Spec::ReplaceElement<LockerSpec, ConcreteElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using Locker          = Instance<LockerSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteLocker  = Instance<ConcreteLockerSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        INFO("Lock observation is valid"
             "\n        of " + Utility::typeName<Subject>()
             + "\nwith owner " + Utility::typeName<Owner>())
        {
            auto literal = literalOf<ConcreteElement>();

            if constexpr (Spec::IsOptional<SubjectSpec>)
            {
                // INFO("Lock of empty " + Utility::typeName<Subject>()
                //        + "\n   with " + Utility::typeName<Locker>())
                {
                    Subject subject;
                    Locker  locker = subject.lock();
                    REQUIRE(!locker);
                }
            }

            // INFO("Lock of " + Utility::typeName<Subject>()
            //        + "\n   with " + Utility::typeName<Locker>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                SourceElement* raw = toAddress(owner);

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                Locker locker = subject.lock();
                O_T_CHECK_NOEXCEPT(locker = subject.lock());
                REQUIRE(isPointTo(owner, raw));
                REQUIRE(isPointTo(subject, raw));
                REQUIRE(isPointTo(locker, raw));

                owner.reset();
                REQUIRE(isEmpty(owner));
                REQUIRE(isPointTo(subject, raw));
                REQUIRE(isPointTo(locker, raw));

                utilize(std::move(subject));
                REQUIRE(isPointTo(locker, raw));
            }

            // INFO("Lock of moved-from " + Utility::typeName<Subject>()
            //        + "\n         with " + Utility::typeName<Locker>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                SourceElement* raw = toAddress(owner);

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                utilize(std::move(subject));
                Locker locker = subject.lock();
                REQUIRE(isEmpty(locker));
                REQUIRE(isPointTo(owner, raw));
            }

            // INFO("Lock of expired " + Utility::typeName<Subject>()
            //        + "\n           with " + Utility::typeName<Locker>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                SourceElement* raw = toAddress(owner);

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                utilize(std::move(owner));
                Locker locker = subject.lock();
                REQUIRE(isEmpty(owner));
                REQUIRE(isEmpty(subject));
                REQUIRE(isEmpty(locker));
            }

            if constexpr (IsDerived<ConcreteElement, SourceElement>)
            {
                // INFO("Lock of derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n   with    base " + Utility::typeName<Locker>())
                {
                    ConcreteOwner    owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    ConcreteElement* raw = toAddress(owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    Locker locker = subject.lock();
                    REQUIRE(isPointTo(owner, raw));
                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(locker, raw));
                }

                // INFO("Lock of derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n   with derived " + Utility::typeName<ConcreteLocker>())
                {
                    ConcreteOwner    owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    ConcreteElement* raw = toAddress(owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    ConcreteLocker locker = subject.lock();
                    REQUIRE(isPointTo(owner, raw));
                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(locker, raw));
                }
            }
        }
    }
};

template <class SubjectSpec>
void lockCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<LockCompatible,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct LockConstIncorrect
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using Subject = Instance<SubjectSpec>;
        using Source  = Instance<SourceSpec>;
        using Locker  = Test::LockerSpec<SubjectSpec>;
        using Owner   = Instance<Conform::OwnerSpec<SourceSpec>>;

        INFO("Lock observation is const incorrect"
             "\n        of " + Utility::typeName<Subject>()
             + "\nwith owner " + Utility::typeName<Owner>())
        {
            // INFO("Lock of " + Utility::typeName<Source>()
            //        + "\n   with " + Utility::typeName<Locker>())
            {
                using SourceElement   = typename SourceSpec::Element;
                using ConcreteElement = Conform::ConcreteOf<SourceElement>;

                Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};
                SourceElement* raw = toAddress(owner);

                auto&& source = forwardOwner<SourceSpec>(owner);

                Locker locker = source.lock();
            }
        }
    #endif
    }
};

template <class SubjectSpec>
void lockConstIncorrect()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<LockConstIncorrect,
          ConstIncorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct LockIncompatible
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectElement         = typename SubjectSpec::Element;
        using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;

        using LockerSpec = Test::LockerSpec<SubjectSpec>;

        using Subject        = Instance<SubjectSpec>;
        using Source         = Instance<SourceSpec>;
        using Locker         = Instance<LockerSpec>;
        using Owner          = Instance<Conform::OwnerSpec<SourceSpec>>;
        using ConcreteLocker = Instance<Spec::ReplaceElement<LockerSpec, ConcreteSubjectElement>>;

        INFO("Lock observation is incompatible"
             "\n        of " + Utility::typeName<Subject>()
             + "\nwith owner " + Utility::typeName<Owner>())
        {
            // INFO("Lock of " + Utility::typeName<Source>()
            //        + "\n   with " + Utility::typeName<Locker>())
            {
                using SourceElement         = typename SourceSpec::Element;
                using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;

                Owner owner{makeOwner<Owner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                SourceElement* raw = toAddress(owner);

                auto&& source = forwardOwner<SourceSpec>(owner);

                Locker locker = source.lock();
            }

        #ifdef ENABLE_COMPILE_ERROR
            if constexpr (IsDerived<ConcreteSubjectElement, SubjectElement>)
            {
                // INFO("Lock of    base " + Utility::typeName<Source>()
                //        + "\n   with derived " + Utility::typeName<ConcreteLocker>())
                {
                    using SourceElement         = typename SourceSpec::Element;
                    using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;

                    Owner owner{makeOwner<Owner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                    SourceElement* raw = toAddress(owner);

                    auto&& source = forwardOwner<SourceSpec>(owner);

                    ConcreteLocker locker = source.lock();
                }
            }

        #endif
        }
    #endif
    }
};

template <class SubjectSpec>
void lockIncompatible()
{
    using namespace Combo::Binary;
    Apply<LockIncompatible, Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct LockObservation<SubjectSpec,
                       std::enable_if_t<Spec::IsLockable<SubjectSpec>>>
{
    void operator()()
    {
        lockCompatible<SubjectSpec>();
        lockConstIncorrect<SubjectSpec>();
        lockIncompatible<SubjectSpec>();
    }
};

template <class SubjectSpec>
struct LockObservation<SubjectSpec,
                       std::enable_if_t<!Spec::IsLockable<SubjectSpec>>>
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using Subject = Instance<SubjectSpec>;
        using Locker  = Test::LockerSpec<SubjectSpec>;

        INFO("Lock observation is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nwith " + Utility::typeName<Locker>())
        {
            Subject subject;
            Locker  locker = subject.lock();
        }
    #endif
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
