/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class SourceSpec, class = void>
struct OwnerBeforeObservation
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectTSpec, class SubjectUSpec, class = void>
struct OwnerBeforeRequire {};

template <class SubjectTSpec, class SubjectUSpec>
struct OwnerBeforeRequire<SubjectTSpec, SubjectUSpec>
{
    using ElementT = typename SubjectTSpec::Element;
    using ElementU = typename SubjectUSpec::Element;

    using SubjectTUSpec  = Spec::ReplaceElement<SubjectTSpec, ElementU>;
    using SubjectUTSpec  = Spec::ReplaceElement<SubjectUSpec, ElementT>;
    using OptionalTUSpec = Spec::ReplaceMultiplicity<SubjectTUSpec, Multiplicity::Optional>;
    using UnifiedTUSpec  = Spec::ReplaceOwnership<OptionalTUSpec, Ownership::Unified>;

    using SubjectT   = Instance<SubjectTSpec>;
    using SubjectU   = Instance<SubjectUSpec>;
    using OptionalTU = Instance<OptionalTUSpec>;
    using UnifiedTU  = Instance<UnifiedTUSpec>;
    using SubjectTU  = Instance<SubjectTUSpec>;
    using SubjectUT  = Instance<SubjectUTSpec>;

    static bool a_before_b(const SubjectT& a, const SubjectU& b)
    { return a.owner_before(b); }

    static bool b_before_a(const SubjectT& a, const SubjectU& b)
    {
        if constexpr (Spec::IsStdSmart<SubjectUSpec>)
        {
            if constexpr (std::is_convertible_v<SubjectT, SubjectUT>)
                return b.owner_before(SubjectUT{a});
            else if constexpr (std::is_convertible_v<SubjectU, SubjectTU>)
                return OptionalTU{b}.owner_before(a);
            else
                return UnifiedTU{b}.owner_before(a);
        }
        else
        {
            return b.owner_before(a);
        }
    }
};

template <class SubjectT, class SubjectU>
bool a_before_b(const SubjectT& a,
                const SubjectU& b)
{
    return OwnerBeforeRequire<Trait::Spec<SubjectT>,
                              Trait::Spec<SubjectU>>::a_before_b(a, b);
}

template <class SubjectT, class SubjectU>
bool b_before_a(const SubjectT& a,
                const SubjectU& b)
{
    return OwnerBeforeRequire<Trait::Spec<SubjectT>,
                              Trait::Spec<SubjectU>>::b_before_a(a, b);
}

template <class SubjectTSpec, class SubjectUSpec>
bool isOwnerEqual(const Instance<SubjectTSpec>& a,
                  const Instance<SubjectUSpec>& b)
{
    return !a_before_b(a, b) && !b_before_a(a, b);
}

template <class SubjectTSpec, class SubjectUSpec>
bool isOwnerUnequal(const Instance<SubjectTSpec>& a,
                    const Instance<SubjectUSpec>& b)
{
    return a_before_b(a, b) != b_before_a(a, b);
}

template <class SubjectTSpec, class SubjectUSpec, class = void>
struct OwnerBefore {};

template <class SubjectTSpec, class SubjectUSpec>
struct OwnerBefore<SubjectTSpec, SubjectUSpec,
                   std::enable_if_t<(  Spec::IsOwnerBased<SubjectTSpec>
                                    && Spec::IsOwnerBased<SubjectUSpec>
                                    && !Spec::IsStdSmart<SubjectTSpec>)>>
{
    void operator()()
    {
        using ElementT         = typename SubjectTSpec::Element;
        using ElementU         = typename SubjectUSpec::Element;
        using ConcreteElementT = Conform::ConcreteOf<ElementT>;
        using ConcreteElementU = Conform::ConcreteOf<ElementU>;

        using OwnerTSpec = Conform::OwnerSpec<SubjectTSpec>;
        using OwnerUSpec = Conform::OwnerSpec<SubjectUSpec>;

        using SubjectT = Instance<SubjectTSpec>;
        using SubjectU = Instance<SubjectUSpec>;
        using OwnerT   = Instance<OwnerTSpec>;
        using OwnerU   = Instance<OwnerUSpec>;

        INFO("OwnerBefore is valid"
             "\n  of " + Utility::typeName<SubjectT>()
             + "\nwith " + Utility::typeName<SubjectU>())
        {
            auto literal_a = literalOf<ConcreteElementT>();
            auto literal_b = literalOf<ConcreteElementU>();

            if constexpr (  Spec::IsOptional<SubjectTSpec>
                         && Spec::IsOptional<SubjectUSpec>)
            {
                // INFO("empty")
                {
                    SubjectT a;
                    SubjectU b;

                    REQUIRE(isOwnerEqual<SubjectTSpec, SubjectUSpec>(a, b));
                }
            }

            // INFO("value")
            {
                OwnerT    owner_a{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
                OwnerU    owner_b{makeOwner<OwnerU, ConcreteElementU>(literal_b)};
                ElementT* raw_a = toAddress(owner_a);
                ElementU* raw_b = toAddress(owner_b);

                auto&& a = forwardOwner<SubjectTSpec>(owner_a);
                auto&& b = forwardOwner<SubjectUSpec>(owner_b);

                REQUIRE(isOwnerUnequal<SubjectTSpec, SubjectUSpec>(a, b));

                cleanup<OwnerTSpec, SubjectTSpec>(raw_a);
                cleanup<OwnerUSpec, SubjectUSpec>(raw_b);
            }

            // INFO("moved-from")
            {
                OwnerT    owner_a_1{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
                OwnerT    owner_a_2{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
                OwnerU    owner_b_1{makeOwner<OwnerU, ConcreteElementU>(literal_b)};
                ElementT* raw_a_1 = toAddress(owner_a_1);
                ElementT* raw_a_2 = toAddress(owner_a_2);
                ElementU* raw_b_1 = toAddress(owner_b_1);

                auto&& a_1 = forwardOwner<SubjectTSpec>(owner_a_1);
                auto&& a_2 = forwardOwner<SubjectTSpec>(owner_a_2);
                auto&& b_1 = forwardOwner<SubjectUSpec>(owner_b_1);

                utilize(std::move(a_1));
            #ifdef TOKEN_TEST_ASSERT
                if constexpr (Spec::IsSingle<SubjectTSpec>)
                {
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        a_before_b(a_1, b_1),
                        "access_to_valued_single()");
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        b_before_a(a_1, b_1),
                        "access_to_valued_single()");
                }
                else
            #endif
                {
                    REQUIRE(isOwnerUnequal<SubjectTSpec, SubjectUSpec>(a_1, b_1));
                }

                utilize(std::move(b_1));
            #ifdef TOKEN_TEST_ASSERT
                if constexpr (Spec::IsSingle<SubjectTSpec>)
                {
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        a_before_b(a_1, b_1),
                        "access_to_valued_single()");
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        b_before_a(a_1, b_1),
                        "access_to_valued_single()");
                }
                else
            #endif
                {
                    REQUIRE(isOwnerEqual<SubjectTSpec, SubjectUSpec>(a_1, b_1));
                    REQUIRE(isOwnerUnequal<SubjectTSpec, SubjectUSpec>(a_2, b_1));
                }

                cleanup<OwnerTSpec, SubjectTSpec>(raw_a_1);
                cleanup<OwnerTSpec, SubjectTSpec>(raw_a_2);
                cleanup<OwnerUSpec, SubjectUSpec>(raw_b_1);
            }
        }
    }
};

template <class SubjectTSpec, class SubjectUSpec>
struct OwnerBefore<SubjectTSpec, SubjectUSpec,
                   std::enable_if_t<(  Spec::IsOwnerBased<SubjectTSpec>
                                    && Spec::IsOwnerBased<SubjectUSpec>
                                    && Spec::IsStdSmart<SubjectTSpec>
                                    && !Spec::IsStdSmart<SubjectUSpec>)>>
{
    void operator()()
    {
        // Same as OwnerBefore<SubjectUSpec, SubjectTSpec>.
    }
};

template <class SubjectTSpec, class SubjectUSpec>
struct OwnerBefore<SubjectTSpec, SubjectUSpec,
                   std::enable_if_t<(  Spec::IsOwnerBased<SubjectTSpec>
                                    && Spec::IsOwnerBased<SubjectUSpec>
                                    && Spec::IsStdSmart<SubjectTSpec>
                                    && Spec::IsStdSmart<SubjectUSpec>)>>
{
    void operator()()
    {
        // Responsibility of the STL.
    }
};

template <class SubjectTSpec, class SubjectUSpec>
struct OwnerBefore<SubjectTSpec, SubjectUSpec,
                   std::enable_if_t<!(  Spec::IsOwnerBased<SubjectTSpec>
                                     && Spec::IsOwnerBased<SubjectUSpec>)>>
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using ElementT         = typename SubjectTSpec::Element;
        using ElementU         = typename SubjectUSpec::Element;
        using ConcreteElementT = Conform::ConcreteOf<ElementT>;
        using ConcreteElementU = Conform::ConcreteOf<ElementU>;

        using OwnerTSpec = Conform::OwnerSpec<SubjectTSpec>;
        using OwnerUSpec = Conform::OwnerSpec<SubjectUSpec>;

        using SubjectT = Instance<SubjectTSpec>;
        using SubjectU = Instance<SubjectUSpec>;
        using OwnerT   = Instance<OwnerTSpec>;
        using OwnerU   = Instance<OwnerUSpec>;

        INFO("OwnerBefore is valid"
             "\n  of " + Utility::typeName<SubjectT>()
             + "\nwith " + Utility::typeName<SubjectU>())
        {
            auto literal_a = literalOf<ConcreteElementT>();
            auto literal_b = literalOf<ConcreteElementU>();

            OwnerT    owner_a{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
            OwnerU    owner_b{makeOwner<OwnerU, ConcreteElementU>(literal_b)};
            ElementT* raw_a = toAddress(owner_a);
            ElementU* raw_b = toAddress(owner_b);

            // INFO("value")
            {
                auto&& a = forwardOwner<SubjectTSpec>(owner_a);
                auto&& b = forwardOwner<SubjectUSpec>(owner_b);

                REQUIRE(a.owner_before(b) != b.owner_before(a));
            }

            cleanup<OwnerTSpec, SubjectTSpec>(raw_a);
            cleanup<OwnerUSpec, SubjectUSpec>(raw_b);
        }
    #endif
    }
};

template <class SubjectTSpec, class SubjectUSpec>
void ownerBefore()
{
    using SpecPair = std::pair<SubjectTSpec, SubjectUSpec>;

    using namespace Combo::Binary;
    Apply<OwnerBefore, ConstSet<SpecPair>>()();
}

} // namespace

template <class SubjectSpec, class SourceSpec>
struct OwnerBeforeObservation<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        ownerBefore<SubjectSpec, SourceSpec>();

        using SourceElement         = typename SourceSpec::Element;
        using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;
        using ConcreteSourceSpec    = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;

        if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
            ownerBefore<SubjectSpec, ConcreteSourceSpec>();

        using IncompatibleElement = Conform::IncompatibleOf<SourceElement>;
        ownerBefore<SubjectSpec, Spec::ReplaceElement<SourceSpec, IncompatibleElement>>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
