/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct ValueObservation
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class SourceSpec, class = void>
struct ValueObservationCompatible
{
    void operator()()
    {
        using SubjectElement  = typename SubjectSpec::Element;
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        INFO("Value observation is valid"
             "\nof " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SourceElement*>() + "}")
        {
            auto literal = literalOf<ConcreteElement>();

        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            REQUIRE(Detector::IsLValueStarDereferenceable<Subject>);
            if constexpr (!std::is_pointer_v<Subject>)
            {
                REQUIRE(Detector::IsLValueArrowDereferenceable<Subject>);

                if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                {
                    REQUIRE(Detector::IsRValueStarDereferenceable<Subject>);
                    REQUIRE(Detector::IsRValueArrowDereferenceable<Subject>);
                }
            }

        #endif

            // INFO("View of value " + Utility::typeName<Subject>()
            //        + "\n         with " + Utility::typeName<SubjectElement>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                const SourceElement* raw = toAddress(owner);
                SubjectElement*      view;

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                view = toAddress(subject);
                O_T_CHECK_NOEXCEPT(view = toAddress(subject));
                REQUIRE(view == raw);

                if constexpr (!std::is_pointer_v<Subject>)
                {
                    view = subject.operator->();
                    O_T_CHECK_NOEXCEPT(view = subject.operator->());
                    REQUIRE(view == raw);
                }

                SubjectElement& ref = *subject;
                O_T_CHECK_NOEXCEPT(*subject);
                REQUIRE(ref == literal);

            #ifdef ENABLE_COMPILE_ERROR
                SubjectElement& temp_ref = *forwardOwner<SubjectSpec>(owner); // using of the temporary

                if constexpr (!std::is_pointer_v<Subject>)
                    SubjectElement* temp_ptr = forwardOwner<SubjectSpec>(owner).operator->(); // using of the temporary

                //
                *subject; // ignoring of the [[nodiscard]]
            #endif

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }

            // INFO("View of moved-from " + Utility::typeName<Subject>()
            //        + "\n         with " + Utility::typeName<SubjectElement>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                const SourceElement* raw = toAddress(owner);
                [[maybe_unused]] SubjectElement* view;

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                if constexpr (Spec::IsElementMoveConstructible<SubjectSpec, SubjectSpec>)
                    utilize(std::move(subject));

                if constexpr (  Spec::IsElementMoveConstructible<SubjectSpec, SubjectSpec>
                             && Spec::IsMoveEmpty<SubjectSpec, SubjectSpec>)
                {
                #ifdef TOKEN_TEST_ASSERT
                    if constexpr (Spec::HasFeature<SubjectSpec,
                                                   Feature::ContractViolation::Abort>)
                    {
                        if constexpr (Spec::IsSingle<SubjectSpec>)
                        {
                            const int answer = 42;
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                view = toAddress(subject),
                                "access_to_valued_single()");
                        }

                        if constexpr (!std::is_pointer_v<Subject>)
                        {
                            const int answer = 42;
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                view = subject.operator->(),
                                "access_to_valued_referrer()");
                        }

                        O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                            SubjectElement& ref = *subject,
                            "access_to_valued_referrer()");
                    }
                    else
                #endif
                    {
                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            view = toAddress(subject));
                        REQUIRE(view == nullptr);

                        if constexpr (!std::is_pointer_v<Subject>)
                        {
                            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                                view = subject.operator->());
                            REQUIRE(view == nullptr);
                        }

                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            [[maybe_unused]] SubjectElement& ref = *subject);
                    }
                }
                else
                {
                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        view = toAddress(subject));
                    REQUIRE(view == raw);

                    if constexpr (!std::is_pointer_v<Subject>)
                    {
                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            view = subject.operator->());
                        REQUIRE(view == raw);
                    }

                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        [[maybe_unused]] SubjectElement& ref = *subject);
                }

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }

            if constexpr (IsDerived<ConcreteElement, SourceElement>)
            {
                // INFO("View of derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n   with    base " + Utility::typeName<SubjectElement>())
                {
                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    const ConcreteElement* raw = toAddress(owner);
                    SubjectElement*        view;

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    view = toAddress(subject);
                    REQUIRE(view == raw);

                    if constexpr (!std::is_pointer_v<Subject>)
                    {
                        view = subject.operator->();
                        REQUIRE(view == raw);
                    }

                    SubjectElement& ref = *subject;
                    REQUIRE(ref == literal);

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }

                // INFO("View of derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n   with derived " + Utility::typeName<ConcreteElement>())
                {
                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    const ConcreteElement* raw = toAddress(owner);
                    ConcreteElement*       view;

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    view = toAddress(subject);
                    REQUIRE(view == raw);

                    if constexpr (!std::is_pointer_v<Subject>)
                    {
                        view = subject.operator->();
                        REQUIRE(view == raw);
                    }

                    ConcreteElement& ref = *subject;
                    REQUIRE(ref == literal);

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }
            }
        }
    }
};

template <class SubjectSpec>
void valueObservationCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ValueObservationCompatible,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ValueObservationConstIncorrect
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectElement = typename SubjectSpec::Element;
        using SourceElement  = typename SourceSpec::Element;

        using SubjectSourceSpec = Spec::ReplaceElement<SubjectSpec, SourceElement>;
        using OwnerSpec         = Conform::OwnerSpec<SubjectSourceSpec>;

        using Subject = Instance<SubjectSourceSpec>;
        using Owner   = Instance<OwnerSpec>;

        INFO("Value observation is const incorrect"
             "\nof " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SourceElement*>() + "}")
        {
            // INFO("View of " + Utility::typeName<Subject>()
            //        + "\n   with " + Utility::typeName<SubjectElement>())
            {
                using ConcreteElement = Conform::ConcreteOf<SourceElement>;

                Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};
                const SourceElement* raw = toAddress(owner);
                SubjectElement*      view;

                auto&& subject = forwardOwner<SubjectSourceSpec>(owner);

                view = toAddress(subject);

                if constexpr (!std::is_pointer_v<Subject>)
                    view = subject.operator->();

                SubjectElement& ref = *subject;
            }
        }
    #endif
    }
};

template <class SubjectSpec>
void valueObservationConstIncorrect()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ValueObservationConstIncorrect,
          ConstIncorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ValueObservationIncompatible
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectElement  = typename SubjectSpec::Element;
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SubjectElement>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;

        using Subject = Instance<SubjectSpec>;
        using Owner   = Instance<OwnerSpec>;

        INFO("Value observation is incompatible"
             "\nof " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SubjectElement*>() + "}")
        {
            // INFO("View of " + Utility::typeName<Subject>()
            //        + "\n   with " + Utility::typeName<SourceElement>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};
                const SubjectElement* raw = toAddress(owner);
                SourceElement*        view;

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                view = toAddress(subject);

                if constexpr (!std::is_pointer_v<Subject>)
                    view = subject.operator->();

                SourceElement& ref = *subject;
            }

        #ifdef ENABLE_COMPILE_ERROR
            if constexpr (IsDerived<ConcreteElement, SubjectElement>)
            {
                // INFO("View of    base " + Utility::typeName<Subject>()
                //        + "\n   with derived " + Utility::typeName<ConcreteElement>())
                {
                    Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};
                    const SubjectElement* raw = toAddress(owner);
                    ConcreteElement*      view;

                    auto&& subject = forwardOwner<SubjectSpec>(owner);

                    view = toAddress(subject);

                    if constexpr (!std::is_pointer_v<Subject>)
                        view = subject.operator->();

                    ConcreteElement& ref = *subject;
                }
            }

        #endif
        }
    #endif
    }
};

template <class SubjectSpec>
void valueObservationIncompatible()
{
    using namespace Combo::Binary;
    Apply<ValueObservationIncompatible,
          Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct ValueObservation<SubjectSpec,
                        std::enable_if_t<Spec::IsDereferenceable<SubjectSpec>>>
{
    void operator()()
    {
        valueObservationCompatible<SubjectSpec>();
        valueObservationConstIncorrect<SubjectSpec>();
        valueObservationIncompatible<SubjectSpec>();
    }
};

template <class SubjectSpec>
struct ValueObservation<SubjectSpec,
                        std::enable_if_t<!Spec::IsDereferenceable<SubjectSpec>>>
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;

    #ifdef TOKEN_TEST_DETECTOR_ENABLED
        REQUIRE(!Detector::IsLValueStarDereferenceable<Subject>);
        REQUIRE(!Detector::IsRValueStarDereferenceable<Subject>);
        REQUIRE(!Detector::IsLValueArrowDereferenceable<Subject>);
        REQUIRE(!Detector::IsRValueArrowDereferenceable<Subject>);
    #endif

    #ifdef ENABLE_COMPILE_ERROR
        using Element = typename SubjectSpec::Element;

        using Subject = Instance<SubjectSpec>;

        INFO("Value observation is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nwith " + Utility::typeName<Element>())
        {
            Element* view;
            Subject  subject;
            view = toAddress(subject);

            if constexpr (!std::is_pointer_v<Subject>)
                view = subject.operator->();

            Element& ref = *subject;
        }
    #endif
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
