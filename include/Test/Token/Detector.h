/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#if __has_include(<experimental/type_traits>)
#include <experimental/type_traits>
#endif

#ifdef __cpp_lib_experimental_detect

#define TOKEN_TEST_DETECTOR_ENABLED

#include <Test/Token/Diagnostics.h>
#include <Test/Token/Token/Trait.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

namespace Test
{

namespace Detector
{

// Construction, assignment
template <class Subject, class Source>
using ExplicitCopyConstructor =
    decltype(Subject { std::declval<const Source&>() });

template <class Subject, class Source>
using ExplicitMoveConstructor =
    decltype(Subject { std::declval<Source &&>() });

template <class Subject, class Source>
using ImplicitCopyAssignment =
    decltype(std::declval<Subject&>() = std::declval<const Source&>());

template <class Subject, class Source>
using ImplicitMoveAssignment =
    decltype(std::declval<Subject&>() = std::declval<Source &&>());

template <class Subject, class Source>
inline constexpr bool IsExplicitCopyConstructible =
    std::experimental::is_detected_v<ExplicitCopyConstructor, Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsExplicitMoveConstructible =
    std::experimental::is_detected_v<ExplicitMoveConstructor, Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsImplicitCopyAssignable =
    std::experimental::is_detected_v<ImplicitCopyAssignment, Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsImplicitMoveAssignable =
    std::experimental::is_detected_v<ImplicitMoveAssignment, Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsCopyConstructible =
    IsExplicitCopyConstructible<Subject, Source>
    && !(  Token::IsExplicitCopy<Subject, Source>
        && IsImplicitCopyAssignable<Subject, Source>);

template <class Subject, class Source>
inline constexpr bool IsNotCopyConstructible =
    !(  IsExplicitCopyConstructible<Subject, Source>
     || IsImplicitCopyAssignable<Subject, Source>);

template <class Subject, class Source>
inline constexpr bool IsMoveConstructible =
    IsExplicitMoveConstructible<Subject, Source>
    && !(  Token::IsExplicitMove<Subject, Source>
        && IsImplicitMoveAssignable<Subject, Source>);

template <class Subject, class Source>
inline constexpr bool IsNotMoveConstructible =
    !(  IsExplicitMoveConstructible<Subject, Source>
     || IsImplicitMoveAssignable<Subject, Source>);

template <class Subject, class Source>
inline constexpr bool IsCopyAssignable =
    Token::IsExplicitCopy<Subject, Source>
    ? !IsImplicitCopyAssignable<Subject, Source>
    : IsImplicitCopyAssignable<Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsNotCopyAssignable =
    !IsImplicitCopyAssignable<Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsMoveAssignable =
    Token::IsExplicitMove<Subject, Source>
    ? !IsImplicitMoveAssignable<Subject, Source>
    : IsImplicitMoveAssignable<Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsNotMoveAssignable =
    !IsImplicitMoveAssignable<Subject, Source>;

// Dereferencing
template <class Subject>
using LValueStarDereferencing =
    decltype(*(std::declval<const Subject&>()));

template <class Subject>
using RValueStarDereferencing =
    decltype(*(std::declval<Subject &&>()));

template <class Subject>
inline constexpr bool IsLValueStarDereferenceable =
    std::experimental::is_detected_v<LValueStarDereferencing, Subject>;

template <class Subject>
inline constexpr bool IsRValueStarDereferenceable =
    std::experimental::is_detected_v<RValueStarDereferencing, Subject>;

template <class Subject>
using LValueArrowDereferencing =
    decltype(std::declval<const Subject&>().operator->());

template <class Subject>
using RValueArrowDereferencing =
    decltype(std::declval<Subject &&>().operator->());

template <class Subject>
inline constexpr bool IsLValueArrowDereferenceable =
    std::experimental::is_detected_v<LValueArrowDereferencing, Subject>;

template <class Subject>
inline constexpr bool IsRValueArrowDereferenceable =
    std::experimental::is_detected_v<RValueArrowDereferencing, Subject>;

// Conversion to bool
template <class Subject>
using LValueBoolConversion =
    decltype(static_cast<bool>(std::declval<const Subject&>()));

template <class Subject>
using RValueBoolConversion =
    decltype(static_cast<bool>(std::declval<Subject &&>()));

template <class Subject>
inline constexpr bool IsLValueBoolConvertible =
    std::experimental::is_detected_v<LValueBoolConversion, Subject>;

template <class Subject>
inline constexpr bool IsRValueBoolConvertible =
    std::experimental::is_detected_v<RValueBoolConversion, Subject>;

// Comparison
template <class Subject, class Source>
using EqualityComparison =
    decltype(std::declval<Subject>() == std::declval<Source>());

template <class Subject, class Source>
using LessComparison =
    decltype(std::declval<Subject>() < std::declval<Source>());

template <class Subject, class Source>
inline constexpr bool IsEqualityComparable =
    std::experimental::is_detected_v<EqualityComparison, Subject, Source>;

template <class Subject, class Source>
inline constexpr bool IsLessComparable =
    std::experimental::is_detected_v<LessComparison, Subject, Source>;

#ifdef __cpp_concepts
#define TOKEN_TEST_DETECTOR_SKIP_SAME(Subject, Source) false
#else
#define TOKEN_TEST_DETECTOR_SKIP_SAME(Subject, Source) \
    std::is_same_v<Subject, Source>
#endif

} // namespace Detector

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END

#endif // __cpp_lib_experimental_detect
