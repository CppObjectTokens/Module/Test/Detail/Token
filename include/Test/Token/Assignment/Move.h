/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class SourceSpec, class = void>
struct MoveAssignment
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class SourceSpec>
void moveToDefaultIsValid()
{
    using SubjectElement         = typename SubjectSpec::Element;
    using SourceElement          = typename SourceSpec::Element;
    using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
    using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

    using OwnerSpec = Conform::OwnerSpec<SourceSpec>;
    using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;
    using ConcreteSourceSpec  = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;
    using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteSourceElement>;

    using Subject         = Instance<SubjectSpec>;
    using Source          = Instance<SourceSpec>;
    using Owner           = Instance<OwnerSpec>;
    using ConcreteSubject = Instance<ConcreteSubjectSpec>;
    using ConcreteSource  = Instance<ConcreteSourceSpec>;
    using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

    INFO("Move assignment to default is valid"
         "\nto   " + Utility::typeName<Subject>()
         + "\nfrom " + Utility::typeName<Source>())
    {
        auto literal = literalOf<ConcreteSourceElement>();

        if constexpr (Spec::IsOptional<SourceSpec>)
        {
            // INFO("Move to   base default " + Utility::typeName<Subject>()
            //        + "\n     from base default " + Utility::typeName<Source>())
            {
                Source  source;
                Subject subject;

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isEmpty(subject));
                REQUIRE(isMoved(source));
            }
        }

        // INFO("Move to   base default " + Utility::typeName<Subject>()
        //        + "\n     from base   value " + Utility::typeName<Source>())
        {
            Owner owner{makeOwner<Owner, ConcreteSourceElement>(literal)};
            SourceElement* raw = toAddress(owner);

            auto&&  source = forwardOwner<SourceSpec>(owner);
            Subject subject;

            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                subject = std::move(source));

            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject, raw));
            REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, raw));

            cleanup<OwnerSpec, SubjectSpec>(raw);
        }

        if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
        {
            // INFO("Move to   base    default " + Utility::typeName<Subject>()
            //        + "\n     from derived   value " + Utility::typeName<ConcreteSource>())
            {
                ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literal)};
                ConcreteSourceElement* raw = toAddress(owner);

                auto&&  source = forwardOwner<ConcreteSourceSpec>(owner);
                Subject subject;

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isPointTo(subject, raw));
                REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, raw));

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }

            // INFO("Move to   derived default " + Utility::typeName<ConcreteSubject>()
            //        + "\n     from derived   value " + Utility::typeName<ConcreteSource>())
            {
                ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literal)};
                ConcreteSourceElement* raw = toAddress(owner);

                auto&& source = forwardOwner<ConcreteSourceSpec>(owner);
                ConcreteSubject subject;

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isPointTo(subject, raw));
                REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, raw));

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }
        }
    }
}

template <class SubjectSpec, class SourceSpec>
void moveToValueIsValid()
{
    using SubjectElement         = typename SubjectSpec::Element;
    using SourceElement          = typename SourceSpec::Element;
    using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
    using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

    using SubjectOwnerSpec         = Conform::OwnerSpec<SubjectSpec>;
    using SourceOwnerSpec          = Conform::OwnerSpec<SourceSpec>;
    using ConcreteSubjectSpec      = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;
    using ConcreteSourceSpec       = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;
    using ConcreteSubjectOwnerSpec = Spec::ReplaceElement<SubjectOwnerSpec, ConcreteSubjectElement>;
    using ConcreteSourceOwnerSpec  = Spec::ReplaceElement<SourceOwnerSpec, ConcreteSourceElement>;

    using Subject              = Instance<SubjectSpec>;
    using Source               = Instance<SourceSpec>;
    using SubjectOwner         = Instance<SubjectOwnerSpec>;
    using SourceOwner          = Instance<SourceOwnerSpec>;
    using ConcreteSubject      = Instance<ConcreteSubjectSpec>;
    using ConcreteSource       = Instance<ConcreteSourceSpec>;
    using ConcreteSubjectOwner = Instance<ConcreteSubjectOwnerSpec>;
    using ConcreteSourceOwner  = Instance<ConcreteSourceOwnerSpec>;

    INFO("Move assignment to value is valid"
         "\nto   " + Utility::typeName<Subject>()
         + "\nfrom " + Utility::typeName<Source>())
    {
    #ifdef TOKEN_TEST_DETECTOR_ENABLED
        REQUIRE(Detector::IsMoveAssignable<Subject, Source>);
    #endif

        auto literal = literalOf<ConcreteSourceElement>();

        // INFO("Move to   base " + Utility::typeName<Subject>()
        //        + "\n     from base " + Utility::typeName<Source>())
        {
            SubjectOwner    subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literal)};
            SubjectElement* subject_raw = toAddress(subject_owner);
            SourceOwner     source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(literal)};
            SourceElement*  source_raw = toAddress(source_owner);

            auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
            auto&& source  = forwardOwner<SourceSpec>(source_owner);

            O_T_CHECK_NOEXCEPT(subject = std::move(source));

            if constexpr (Spec::IsMoveNoexcept<SubjectSpec, SourceSpec>)
            {
                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject, source_raw));
                REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, source_raw));

                // Move assignment from nullptr is valid.
                if constexpr (Spec::IsPointerConstructible<SubjectSpec>)
                {
                    if constexpr (Spec::IsOptional<SubjectSpec>)
                    {
                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            subject = std::move(nullptr));
                        O_T_CHECK_NOEXCEPT(subject = std::move(nullptr));

                        REQUIRE(isEmpty(subject));
                    }
                    else
                    {
                    #ifdef ENABLE_COMPILE_ERROR
                        subject = std::move(nullptr);
                        REQUIRE(isEmpty(subject));
                    #endif
                    }
                }
            }
            else
            {
                using ExceptionType = Bind::Rule::MoveException<SubjectSpec, SourceSpec>;
                REQUIRE_THROWS_AS([&] { subject = std::move(source); } (),
                                  ExceptionType);
            }

            cleanup<SubjectOwnerSpec, SubjectSpec>(subject_raw);
            cleanup<SourceOwnerSpec, SourceSpec>(  source_raw);
        }

        if constexpr (Spec::IsTolerateMovedFrom<SubjectSpec, SourceSpec>)
        {
            // INFO("Move to   base " + Utility::typeName<Subject>()
            //        + "\n     from base moved-from " + Utility::typeName<Source>())
            {
                SubjectOwner    subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literal)};
                SubjectElement* subject_raw = toAddress(subject_owner);
                SourceOwner     source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(literal)};
                SourceElement*  source_raw = toAddress(source_owner);

                auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                auto&& source  = forwardOwner<SourceSpec>(source_owner);

                utilize(std::move(source));
                REQUIRE(isMoved(source));

            #ifdef TOKEN_TEST_ASSERT
                if constexpr (Spec::IsSingle<SourceSpec>)
                {
                    const int answer = 42;
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        subject = std::move(source),
                        "access_to_valued_single()");
                }
                else if constexpr (Spec::IsSingle<SubjectSpec>)
                {
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        subject = std::move(source),
                        "construct_from_valued_referrer(source)");
                }
                else
            #endif
                {
                    if constexpr (Spec::IsMoveNoexcept<SubjectSpec, SourceSpec>)
                    {
                    #ifndef NDEBUG
                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            subject = std::move(source));

                        REQUIRE(isEmpty(subject));
                    #endif
                    }
                    else
                    {
                        using ExceptionType = Bind::Rule::MoveException<SubjectSpec, SourceSpec>;
                        REQUIRE_THROWS_AS([&] { subject = std::move(source); } (),
                                          ExceptionType);
                    }
                }

                cleanup<SubjectOwnerSpec, SubjectSpec>(subject_raw);
                cleanup<SourceOwnerSpec, SourceSpec>(  source_raw);
            }
        }

        // INFO("Move to   moved-from " + Utility::typeName<Subject>()
        //        + "\n     from       base " + Utility::typeName<Source>())
        {
            SubjectOwner    subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
            SubjectElement* subject_raw = toAddress(subject_owner);
            SourceOwner     source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
            SourceElement*  source_raw = toAddress(source_owner);

            auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
            auto&& source  = forwardOwner<SourceSpec>(source_owner);

            utilize(std::move(subject));
            REQUIRE(isMoved<SubjectSpec, SubjectSpec>(subject, subject_raw));

            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                subject = std::move(source));

            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject, source_raw));
            REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, source_raw));

            cleanup<SubjectOwnerSpec, SubjectSpec>(subject_raw);
            cleanup<SourceOwnerSpec, SourceSpec>(  source_raw);
        }

        if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
        {
            // INFO("Move to      base " + Utility::typeName<Subject>()
            //        + "\n     from derived " + Utility::typeName<ConcreteSource>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                REQUIRE(Detector::IsMoveAssignable<Subject, ConcreteSource>);
            #endif

                SubjectOwner           subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literal)};
                SubjectElement*        subject_raw = toAddress(subject_owner);
                ConcreteSourceOwner    source_owner{makeOwner<ConcreteSourceOwner, ConcreteSourceElement>(literal)};
                ConcreteSourceElement* source_raw = toAddress(source_owner);

                auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                auto&& source  = forwardOwner<ConcreteSourceSpec>(source_owner);

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isPointTo(subject, source_raw));
                REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, source_raw));

                cleanup<SubjectOwnerSpec, SubjectSpec>(subject_raw);
                cleanup<SourceOwnerSpec, SourceSpec>(  source_raw);
            }

            // INFO("Move to   derived " + Utility::typeName<ConcreteSubject>()
            //        + "\n     from derived " + Utility::typeName<ConcreteSource>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                REQUIRE(Detector::IsMoveAssignable<ConcreteSubject, ConcreteSource>);
            #endif

                ConcreteSubjectOwner    subject_owner{makeOwner<ConcreteSubjectOwner, ConcreteSubjectElement>(literal)};
                ConcreteSubjectElement* subject_raw = toAddress(subject_owner);
                ConcreteSourceOwner     source_owner{makeOwner<ConcreteSourceOwner, ConcreteSourceElement>(literal)};
                ConcreteSourceElement*  source_raw = toAddress(source_owner);

                auto&& subject = forwardOwner<ConcreteSubjectSpec>(subject_owner);
                auto&& source  = forwardOwner<ConcreteSourceSpec>(source_owner);

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    subject = std::move(source));

                REQUIRE(isPointTo(subject, source_raw));
                REQUIRE(isMoved<SubjectSpec, SourceSpec>(source, source_raw));

                cleanup<SubjectOwnerSpec, SubjectSpec>(subject_raw);
                cleanup<SourceOwnerSpec, SourceSpec>(  source_raw);
            }
        }
    }
}

template <class SubjectSpec, class SourceSpec, class = void>
struct MoveAssignmentIsValid
{
    void operator()()
    {
        if constexpr (Spec::IsOptional<SubjectSpec>)
            moveToDefaultIsValid<SubjectSpec, SourceSpec>();

        moveToValueIsValid<SubjectSpec, SourceSpec>();
    }
};

template <class SubjectSpec, class SourceSpec, class = void>
struct MoveAssignmentIsInvalid {};

template <class SubjectSpec, class SourceSpec>
struct MoveAssignmentIsInvalid<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        using SubjectElement         = typename SubjectSpec::Element;
        using SourceElement          = typename SourceSpec::Element;
        using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
        using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

        using SubjectOwnerSpec         = Conform::OwnerSpec<SubjectSpec>;
        using SourceOwnerSpec          = Conform::OwnerSpec<SourceSpec>;
        using ConcreteSubjectSpec      = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;
        using ConcreteSourceSpec       = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;
        using ConcreteSubjectOwnerSpec = Spec::ReplaceElement<SubjectOwnerSpec, ConcreteSubjectElement>;
        using ConcreteSourceOwnerSpec  = Spec::ReplaceElement<SourceOwnerSpec, ConcreteSourceElement>;

        using Subject              = Instance<SubjectSpec>;
        using Source               = Instance<SourceSpec>;
        using SubjectOwner         = Instance<SubjectOwnerSpec>;
        using SourceOwner          = Instance<SourceOwnerSpec>;
        using ConcreteSubject      = Instance<ConcreteSubjectSpec>;
        using ConcreteSource       = Instance<ConcreteSourceSpec>;
        using ConcreteSubjectOwner = Instance<ConcreteSubjectOwnerSpec>;
        using ConcreteSourceOwner  = Instance<ConcreteSourceOwnerSpec>;

        INFO("Move assignment is invalid"
             "\nto   " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<Source>())
        {
            // INFO("Move to   base value " + Utility::typeName<Subject>()
            //        + "\n     from base value " + Utility::typeName<Source>()
            //        + "\n          with subject owner " + Utility::typeName<SubjectOwner>()
            //        + "\n          with source  owner " + Utility::typeName<SourceOwner>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                if constexpr (!TOKEN_TEST_DETECTOR_SKIP_SAME(Subject, Source)) // <- Remove in C++20
                    REQUIRE(Detector::IsNotMoveAssignable<Subject, Source>);

            #endif

            #ifdef ENABLE_COMPILE_ERROR
                SubjectOwner    subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
                SubjectElement* subject_raw = toAddress(subject_owner);
                SourceOwner     source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                SourceElement*  source_raw = toAddress(source_owner);

                auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                auto&& source  = forwardOwner<SourceSpec>(source_owner);
                subject = std::move(source);
            #endif
            }

            if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
            {
                // INFO("Move to   base    value " + Utility::typeName<Subject>()
                //        + "\n     from derived value " + Utility::typeName<ConcreteSource>()
                //        + "\n             with subject owner " + Utility::typeName<SubjectOwner>()
                //        + "\n             with source  owner " + Utility::typeName<ConcreteSourceOwner>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    REQUIRE(Detector::IsNotMoveAssignable<Subject, ConcreteSource>);
                #endif

                #ifdef ENABLE_COMPILE_ERROR
                    SubjectOwner           subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
                    SubjectElement*        subject_raw = toAddress(subject_owner);
                    ConcreteSourceOwner    source_owner{makeOwner<ConcreteSourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                    ConcreteSourceElement* source_raw = toAddress(source_owner);

                    auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
                    auto&& source  = forwardOwner<ConcreteSourceSpec>(source_owner);
                    subject = std::move(source);
                #endif
                }

                // INFO("Move to   derived value " + Utility::typeName<ConcreteSubject>()
                //        + "\n     from derived value " + Utility::typeName<ConcreteSource>()
                //        + "\n             with subject owner " + Utility::typeName<ConcreteSubjectOwner>()
                //        + "\n             with source  owner " + Utility::typeName<ConcreteSourceOwner>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    if constexpr (!TOKEN_TEST_DETECTOR_SKIP_SAME(ConcreteSubject, ConcreteSource)) // <- Remove in C++20
                        REQUIRE(Detector::IsNotMoveAssignable<ConcreteSubject, ConcreteSource>);

                #endif

                #ifdef ENABLE_COMPILE_ERROR
                    ConcreteSubjectOwner    subject_owner{makeOwner<ConcreteSubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
                    ConcreteSubjectElement* subject_raw = toAddress(subject_owner);
                    ConcreteSourceOwner     source_owner{makeOwner<ConcreteSourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                    ConcreteSourceElement*  source_raw = toAddress(source_owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(subject_owner);
                    auto&& source  = forwardOwner<ConcreteSourceSpec>(source_owner);
                    subject = std::move(source);
                #endif
                }
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec, class = void>
struct MoveAssignmentElementIsValid
{
    void operator()()
    {
        if constexpr (Spec::IsElementMoveAssignable<SubjectSpec, SourceSpec>)
            MoveAssignmentIsValid<SubjectSpec, SourceSpec>()();
        else
            MoveAssignmentIsInvalid<SubjectSpec, SourceSpec>()();
    }
};

template <class SubjectSpec, class SourceSpec>
void moveAssignmentIsValid()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<MoveAssignmentElementIsValid, ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec>
void moveAssignmentIsInvalid()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<MoveAssignmentIsInvalid, ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct MoveAssignmentIncompatible
{
    void operator()()
    {
        using SubjectElement         = typename SubjectSpec::Element;
        using SourceElement          = typename SourceSpec::Element;
        using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
        using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

        using SubjectOwnerSpec    = Conform::OwnerSpec<SubjectSpec>;
        using SourceOwnerSpec     = Conform::OwnerSpec<SourceSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;

        using Subject         = Instance<SubjectSpec>;
        using SubjectOwner    = Instance<SubjectOwnerSpec>;
        using SourceOwner     = Instance<SourceOwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;

        INFO("Move assignment is incompatible"
             "\n  to " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceOwner>())
        {
        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            REQUIRE(Detector::IsNotMoveAssignable<Subject, SourceOwner>);
        #endif

        #ifdef ENABLE_COMPILE_ERROR
            SubjectOwner subject_owner{makeOwner<SubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
            SourceOwner  source_owner{makeOwner<SourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};

            auto&& subject = forwardOwner<SubjectSpec>(subject_owner);
            subject = std::move(source_owner);
        #endif
        }

        if constexpr (IsDerived<ConcreteSubjectElement, SubjectElement>)
        {
            INFO("Move assignment is incompatible"
                 "\n  to derived " + Utility::typeName<ConcreteSubject>()
                 + "\nfrom base    " + Utility::typeName<SubjectOwner>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                REQUIRE(Detector::IsNotMoveAssignable<ConcreteSubject, SubjectOwner>);
            #endif

            #ifdef ENABLE_COMPILE_ERROR
                using SourceOwner = SubjectOwner;
                using ConcreteSubjectOwner = Instance<Spec::ReplaceElement<SubjectOwnerSpec, ConcreteSubjectElement>>;

                ConcreteSubjectOwner subject_owner{makeOwner<ConcreteSubjectOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};
                SourceOwner source_owner{makeOwner<SourceOwner, ConcreteSubjectElement>(literalOf<ConcreteSubjectElement>())};

                auto&& subject = forwardOwner<ConcreteSubjectSpec>(subject_owner);
                subject = std::move(source_owner);
            #endif
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec>
void moveAssignmentIncompatible()
{
    using namespace Combo::Binary;
    Apply<MoveAssignmentIncompatible,
          IncompatibleSet<SubjectSpec, SourceSpec>>()();
}

} // namespace

template <class SubjectSpec, class SourceSpec>
struct MoveAssignment<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        if constexpr (  Spec::IsMovable<SubjectSpec, SourceSpec>
                     && !Spec::IsExplicitCopy<SubjectSpec, SourceSpec>)
            moveAssignmentIsValid<SubjectSpec, SourceSpec>();
        else
            moveAssignmentIsInvalid<SubjectSpec, SourceSpec>();

        moveAssignmentIncompatible<SubjectSpec, SourceSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
