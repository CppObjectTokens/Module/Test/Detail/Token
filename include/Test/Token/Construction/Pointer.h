/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct PointerConstruction
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class SourceSpec, class = void>
struct PointerConstructionCompatible
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<Spec::ReplaceElement<SubjectSpec, ConcreteElement>>;

        if constexpr (Spec::IsSingle<SubjectSpec>)
        {
            INFO("Pointer construction is invalid"
                 "\n  of " + Utility::typeName<Subject>()
                 + "\nfrom nullptr")
            {
                REQUIRE(!std::is_constructible_v<Subject, std::nullptr_t>);

            #ifdef ENABLE_COMPILE_ERROR
                {
                    Subject subject{nullptr};
                    REQUIRE(isEmpty(subject));
                }
                {
                    Subject subject = nullptr;
                    REQUIRE(isEmpty(subject));
                }
            #endif
            }
        }
        else
        {
            INFO("Pointer construction is valid"
                 "\n  of " + Utility::typeName<Subject>()
                 + "\nfrom nullptr")
            {
                REQUIRE(std::is_constructible_v<Subject, std::nullptr_t>);

                {
                    Subject subject{nullptr};
                    O_T_CHECK_NOEXCEPT(Subject{nullptr});

                    REQUIRE(isEmpty(subject));
                }
                {
                    Subject subject = nullptr;
                    REQUIRE(isEmpty(subject));
                }
            }
        }

        INFO("Pointer construction is valid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            auto literal = literalOf<ConcreteElement>();

            // INFO("Init of base " + Utility::typeName<Subject>()
            //        + "\n   from base " + Utility::typeName<SourceElement*>())
            {
                // REQUIRE(std::is_constructible_v<Subject, SourceElement*>);

                SourceElement* raw = new ConcreteElement(literal);
                {
                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        Subject subject{raw});
                    O_T_CHECK_NOEXCEPT(Subject{raw});

                    REQUIRE(isPointTo(subject, raw));
                }
                {
                #ifdef ENABLE_COMPILE_ERROR
                    Subject subject = raw;
                    REQUIRE(isPointTo(subject, raw));
                #endif
                }

                cleanup<SubjectSpec, SubjectSpec>(raw);
            }

            // INFO("Init of base " + Utility::typeName<Subject>()
            //        + "\n   from null " + Utility::typeName<SourceElement*>())
            {
                SourceElement* raw = nullptr;

                if constexpr (Spec::IsSingle<SubjectSpec>)
                {
                    O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                        Subject subject{raw},
                        "construct_from_valued_referrer(source)");
                }
                else
                {
                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        Subject subject{raw});

                    REQUIRE(isEmpty(subject));
                }
            }

            if constexpr (IsDerived<ConcreteElement, SourceElement>)
            {
                // INFO("Init of    base " + Utility::typeName<Subject>()
                //        + "\n   from derived " + Utility::typeName<ConcreteElement*>())
                {
                    // REQUIRE(std::is_constructible_v<Subject, ConcreteElement*>);

                    ConcreteElement* raw = new ConcreteElement(literal);
                    Subject subject{raw};
                    REQUIRE(isPointTo(subject, raw));

                    cleanup<SubjectSpec, SubjectSpec>(raw);
                }

                // INFO("Init of derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n   from derived " + Utility::typeName<ConcreteElement*>())
                {
                    // REQUIRE(std::is_constructible_v<ConcreteSubject, ConcreteElement*>);

                    ConcreteElement* raw = new ConcreteElement(literal);
                    ConcreteSubject  subject{raw};
                    REQUIRE(isPointTo(subject, raw));

                    cleanup<SubjectSpec, SubjectSpec>(raw);
                }
            }
        }
    }
};

template <class SubjectSpec>
void pointerConstructionCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<PointerConstructionCompatible,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct PointerConstructionIsInvalid
{
    void operator()()
    {
        using SourceElement = typename SourceSpec::Element;

        using Subject = Instance<SubjectSpec>;

        INFO("Pointer construction is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom nullptr")
        {
            REQUIRE(!std::is_constructible_v<Subject, std::nullptr_t>);

            #ifdef ENABLE_COMPILE_ERROR
            {
                Subject subject{nullptr};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = nullptr;
                REQUIRE(isEmpty(subject));
            }
            #endif
        }

        INFO("Pointer construction is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            // INFO("Init of " + Utility::typeName<Subject>()
            //        + "\n   from " + Utility::typeName<SourceElement*>())
            {
                REQUIRE(!std::is_constructible_v<Subject, SourceElement*>);

                #ifdef ENABLE_COMPILE_ERROR
                using ConcreteElement = Conform::ConcreteOf<SourceElement>;

                auto literal = literalOf<ConcreteElement>();

                ConcreteElement* raw = new ConcreteElement(literal);

                {
                    Subject subject{raw};
                    REQUIRE(isEmpty(subject));
                }
                {
                    Subject subject = raw;
                    REQUIRE(isEmpty(subject));
                }
                #endif
            }
        }
    }
};

template <class SubjectSpec>
void pointerConstructionIsInvalid()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<PointerConstructionIsInvalid,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct PointerConstructionConstIncorrect
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using Subject = Instance<SubjectSpec>;

        INFO("Pointer construction is const incorrect"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            // INFO("Init of base " + Utility::typeName<Subject>()
            //        + "\n   from base " + Utility::typeName<SourceElement*>())
            {
                REQUIRE(!std::is_constructible_v<Subject, SourceElement*>);

            #ifdef ENABLE_COMPILE_ERROR
                auto literal = literalOf<ConcreteElement>();

                SourceElement* raw = new ConcreteElement(literal);
                Subject        subject{raw};
            #endif
            }

            if constexpr (IsDerived<ConcreteElement, SourceElement>)
            {
                // INFO("Init of    base " + Utility::typeName<Subject>()
                //        + "\n   from derived " + Utility::typeName<ConcreteElement*>())
                {
                    REQUIRE(!std::is_constructible_v<Subject, ConcreteElement*>);

                #ifdef ENABLE_COMPILE_ERROR
                    auto literal = literalOf<ConcreteElement>();

                    ConcreteElement* raw = new ConcreteElement(literal);
                    Subject subject{raw};
                #endif
                }
            }
        }
    }
};

template <class SubjectSpec>
void pointerConstructionConstIncorrect()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<PointerConstructionConstIncorrect,
          ConstIncorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct PointerConstructionIncompatible
{
    void operator()()
    {
        using SubjectElement  = typename SubjectSpec::Element;
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SubjectElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<Spec::ReplaceElement<SubjectSpec, ConcreteElement>>;

        INFO("Pointer construction is incompatible"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement*>())
        {
            REQUIRE(!std::is_constructible_v<Subject, SourceElement*>);

        #ifdef ENABLE_COMPILE_ERROR
            using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;

            auto literal = literalOf<ConcreteSourceElement>();

            SourceElement* raw = new ConcreteSourceElement(literal);
            Subject        subject{raw};
        #endif
        }

        if constexpr (IsDerived<ConcreteElement, SubjectElement>)
        {
            // INFO("Pointer construction is incompatible"
            //        "\n  of derived " + Utility::typeName<ConcreteSubject>()
            //        + "\nfrom    base " + Utility::typeName<SubjectElement*>())
            {
                REQUIRE(!std::is_constructible_v<ConcreteSubject, SubjectElement*>);

            #ifdef ENABLE_COMPILE_ERROR
                SubjectElement* raw = nullptr;
                ConcreteSubject subject{raw};
            #endif
            }
        }
    }
};

template <class SubjectSpec>
void pointerConstructionIncompatible()
{
    using namespace Combo::Binary;
    Apply<PointerConstructionIncompatible,
          Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct PointerConstruction<SubjectSpec>
{
    void operator()()
    {
        if constexpr (Spec::IsPointerConstructible<SubjectSpec>)
        {
            pointerConstructionCompatible<SubjectSpec>();
            pointerConstructionConstIncorrect<SubjectSpec>();
            pointerConstructionIncompatible<SubjectSpec>();
        }
        else
        {
            pointerConstructionIsInvalid<SubjectSpec>();
        }
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
