/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class SourceSpec, class = void>
struct CopyConstruction
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class SourceSpec, class = void>
struct CopyConstructionIsValid {};

template <class SubjectSpec, class SourceSpec>
struct CopyConstructionIsValid<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        using SubjectElement         = typename SubjectSpec::Element;
        using SourceElement          = typename SourceSpec::Element;
        using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
        using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

        using OwnerSpec = Conform::OwnerSpec<SourceSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;
        using ConcreteSourceSpec  = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteSourceElement>;

        using Subject         = Instance<SubjectSpec>;
        using Source          = Instance<SourceSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteSource  = Instance<ConcreteSourceSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        INFO("Copy construction is valid"
             "\nof   " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<Source>())
        {
            auto literal = literalOf<ConcreteSourceElement>();

            // INFO("Init of   base       " + Utility::typeName<Subject>()
            //        + "\n     from base value " + Utility::typeName<Source>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                REQUIRE(Detector::IsCopyConstructible<Subject, Source>);
            #endif

                Owner owner{makeOwner<Owner, ConcreteSourceElement>(literal)};
                SourceElement* raw = toAddress(owner);

                auto&&  source  = forwardOwner<SourceSpec>(owner);
                Subject subject = forwardExplicit<Subject>(source);
                O_T_CHECK_NOEXCEPT(Subject{forwardExplicit<Subject>(source)});

                REQUIRE(isPointTo(subject, raw));
                REQUIRE(isPointTo(source, raw));

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }

            if constexpr (Spec::IsTolerateMovedFrom<SubjectSpec, SourceSpec>)
            {
                // INFO("Init of   base       " + Utility::typeName<Subject>()
                //        + "\n     from base moved-from " + Utility::typeName<Source>())
                {
                    Owner owner{makeOwner<Owner, ConcreteSourceElement>(literal)};
                    SourceElement* raw = toAddress(owner);

                    auto&& source = forwardOwner<SourceSpec>(owner);

                    utilize(std::move(source));
                    REQUIRE(isMoved(source));

                #ifdef TOKEN_TEST_ASSERT
                    if constexpr (Spec::IsSingle<SourceSpec>)
                    {
                        O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                            [[maybe_unused]] Subject subject = forwardExplicit<Subject>(source),
                            "access_to_valued_single()");
                    }
                    else if constexpr (Spec::IsSingle<SubjectSpec>)
                    {
                        O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                            [[maybe_unused]] Subject subject = forwardExplicit<Subject>(source),
                            "construct_from_valued_referrer(source)");
                    }
                    else
                #endif
                    {
                        if constexpr (Spec::IsCopyNoexcept<SubjectSpec, SourceSpec>)
                        {
                        #ifndef NDEBUG
                            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                                Subject subject = forwardExplicit<Subject>(source));

                            REQUIRE(isEmpty(subject));
                        #endif
                        }
                        else
                        {
                            using ExceptionType = Bind::Rule::CopyException<SubjectSpec, SourceSpec>;
                            REQUIRE_THROWS_AS([&] { Subject subject = forwardExplicit<Subject>(source); } (),
                                              ExceptionType);
                        }
                    }

                    cleanup<OwnerSpec, SourceSpec>(raw);
                }
            }

            if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
            {
                // INFO("Init of   base    " + Utility::typeName<Subject>()
                //        + "\n     from derived " + Utility::typeName<ConcreteSource>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    REQUIRE(Detector::IsCopyConstructible<Subject, ConcreteSource>);
                #endif

                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literal)};
                    ConcreteSourceElement* raw = toAddress(owner);

                    auto&&  source  = forwardOwner<SourceSpec>(owner);
                    Subject subject = forwardExplicit<Subject>(source);

                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(source, raw));

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }

                // INFO("Init of   derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n     from derived " + Utility::typeName<ConcreteSource>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    REQUIRE(Detector::IsCopyConstructible<ConcreteSubject, ConcreteSource>);
                #endif

                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literal)};
                    ConcreteSourceElement* raw = toAddress(owner);

                    auto&& source = forwardOwner<ConcreteSourceSpec>(owner);
                    ConcreteSubject subject = forwardExplicit<ConcreteSubject>(source);

                    REQUIRE(isPointTo(subject, raw));
                    REQUIRE(isPointTo(source, raw));

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec, class = void>
struct CopyConstructionIsInvalid {};

template <class SubjectSpec, class SourceSpec>
struct CopyConstructionIsInvalid<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        using SubjectElement         = typename SubjectSpec::Element;
        using SourceElement          = typename SourceSpec::Element;
        using ConcreteSubjectElement = Conform::ConcreteOf<SubjectElement>;
        using ConcreteSourceElement  = Conform::ConcreteOf<SourceElement>;

        using OwnerSpec = Conform::OwnerSpec<SourceSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteSubjectElement>;
        using ConcreteSourceSpec  = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteSourceElement>;

        using Subject         = Instance<SubjectSpec>;
        using Source          = Instance<SourceSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteSource  = Instance<ConcreteSourceSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        INFO("Copy construction is invalid"
             "\nof   " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<Source>())
        {
            // INFO("Init of   base       " + Utility::typeName<Subject>()
            //        + "\n     from base value " + Utility::typeName<Source>()
            //        + "\n          with owner " + Utility::typeName<Owner>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                if constexpr (!TOKEN_TEST_DETECTOR_SKIP_SAME(Subject, Source)) // <- Remove in C++20
                    REQUIRE(Detector::IsNotCopyConstructible<Subject, Source>);

            #endif

            #ifdef ENABLE_COMPILE_ERROR
                Owner   owner{makeOwner<Owner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                auto&&  source = forwardOwner<SourceSpec>(owner);
                Subject subject{source};
            #endif
            }

            if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
            {
                // INFO("Init of   base    " + Utility::typeName<Subject>()
                //        + "\n     from derived " + Utility::typeName<ConcreteSource>()
                //        + "\n     with owner   " + Utility::typeName<ConcreteOwner>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    REQUIRE(Detector::IsNotCopyConstructible<Subject, ConcreteSource>);
                #endif

                #ifdef ENABLE_COMPILE_ERROR
                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                    auto&&        source = forwardOwner<ConcreteSourceSpec>(owner);
                    Subject       subject{source};
                #endif
                }

                // INFO("Init of   derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n     from derived " + Utility::typeName<ConcreteSource>()
                //        + "\n     with owner   " + Utility::typeName<ConcreteOwner>())
                {
                #ifdef TOKEN_TEST_DETECTOR_ENABLED
                    if constexpr (!TOKEN_TEST_DETECTOR_SKIP_SAME(ConcreteSubject, ConcreteSource)) // <- Remove in C++20
                        REQUIRE(Detector::IsNotCopyConstructible<ConcreteSubject, ConcreteSource>);

                #endif

                #ifdef ENABLE_COMPILE_ERROR
                    ConcreteOwner   owner{makeOwner<ConcreteOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
                    auto&&          source = forwardOwner<ConcreteSourceSpec>(owner);
                    ConcreteSubject subject{source};
                #endif
                }
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec, class = void>
struct CopyConstructionElementIsValid
{
    void operator()()
    {
        if constexpr (Spec::IsElementCopyable<SubjectSpec, SourceSpec>)
            CopyConstructionIsValid<SubjectSpec, SourceSpec>()();
        else
            CopyConstructionIsInvalid<SubjectSpec, SourceSpec>()();
    }
};

template <class SubjectSpec, class SourceSpec>
void copyConstructionIsValid()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<CopyConstructionElementIsValid, ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec>
void copyConstructionIsInvalid()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<CopyConstructionIsInvalid, ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct CopyConstructionIncompatible
{
    void operator()()
    {
        using SubjectElement  = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SubjectElement>;

        using Subject         = Instance<SubjectSpec>;
        using SubjectOwner    = Instance<Conform::OwnerSpec<SubjectSpec>>;
        using SourceOwner     = Instance<Conform::OwnerSpec<SourceSpec>>;
        using ConcreteSubject = Instance<Spec::ReplaceElement<SubjectSpec, ConcreteElement>>;

        INFO("Copy construction is incompatible"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceOwner>())
        {
        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            REQUIRE(Detector::IsNotCopyConstructible<Subject, SourceOwner>);
        #endif

        #ifdef ENABLE_COMPILE_ERROR
            using SourceElement         = typename SourceSpec::Element;
            using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;

            SourceOwner source{makeOwner<SourceOwner, ConcreteSourceElement>(literalOf<ConcreteSourceElement>())};
            Subject     subject{source};
        #endif
        }

        if constexpr (IsDerived<ConcreteElement, SubjectElement>)
        {
            INFO("Copy construction is incompatible"
                 "\n  of derived " + Utility::typeName<ConcreteSubject>()
                 + "\nfrom base    " + Utility::typeName<SubjectOwner>())
            {
            #ifdef TOKEN_TEST_DETECTOR_ENABLED
                REQUIRE(Detector::IsNotCopyConstructible<ConcreteSubject, SubjectOwner>);
            #endif

            #ifdef ENABLE_COMPILE_ERROR
                using Source = SubjectOwner;

                Source source{makeOwner<Source, ConcreteElement>(literalOf<ConcreteElement>())};
                ConcreteSubject subject{source};
            #endif
            }
        }
    }
};

template <class SubjectSpec, class SourceSpec>
void copyConstructionIncompatible()
{
    using namespace Combo::Binary;
    Apply<CopyConstructionIncompatible,
          IncompatibleSet<SubjectSpec, SourceSpec>>()();
}

} // namespace

template <class SubjectSpec, class SourceSpec>
struct CopyConstruction<SubjectSpec, SourceSpec>
{
    void operator()()
    {
        if constexpr (Spec::IsCopyable<SubjectSpec, SourceSpec>)
            copyConstructionIsValid<SubjectSpec, SourceSpec>();
        else
            copyConstructionIsInvalid<SubjectSpec, SourceSpec>();

        copyConstructionIncompatible<SubjectSpec, SourceSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
