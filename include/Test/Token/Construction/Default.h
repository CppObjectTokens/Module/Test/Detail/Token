/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct DefaultConstruction
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class = void>
struct DefaultIsValid
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;

        INFO("Default construction is valid"
             "\nof " + Utility::typeName<Subject>())
        {
            REQUIRE(std::is_default_constructible_v<Subject>);

            if constexpr (!Spec::IsMultiplicityUnknown<SubjectSpec>)
            {
                Subject subject;
                REQUIRE(isEmpty(subject));
            }

            {
                Subject subject{};
                O_T_CHECK_NOEXCEPT(Subject{});
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = {};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = Subject{};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = {Subject{}};
                REQUIRE(isEmpty(subject));
            }
        }
    }
};

template <class SubjectSpec>
void defaultIsValid()
{
    using namespace Combo::Unary;
    Apply<DefaultIsValid,
          ConstSet<SubjectSpec>>()();
}

template <class SubjectSpec, class = void>
struct DefaultIsInvalid
{
    void operator()()
    {
        using Subject = Instance<SubjectSpec>;

        INFO("Default construction is invalid"
             "\nof " + Utility::typeName<Subject>())
        {
            REQUIRE(!std::is_default_constructible_v<Subject>);

        #ifdef ENABLE_COMPILE_ERROR
            {
                Subject subject;
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject{};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = {};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = Subject{};
                REQUIRE(isEmpty(subject));
            }
            {
                Subject subject = {Subject{}};
                REQUIRE(isEmpty(subject));
            }
        #endif
        }
    }
};

template <class SubjectSpec>
void defaultIsInvalid()
{
    using namespace Combo::Unary;
    Apply<DefaultIsInvalid,
          ConstSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct DefaultConstruction<SubjectSpec>
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;
        using ConcreteSpec    = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        if constexpr (Spec::IsSingle<SubjectSpec>)
        {
            defaultIsInvalid<SubjectSpec>();

            if constexpr (IsDerived<ConcreteElement, Element>)
                defaultIsInvalid<ConcreteSpec>();
        }
        else
        {
            defaultIsValid<SubjectSpec>();

            if constexpr (IsDerived<ConcreteElement, Element>)
                defaultIsValid<ConcreteSpec>();
        }
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
