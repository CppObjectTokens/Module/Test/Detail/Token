/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct ItselfConstruction
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class SourceSpec, class = void>
struct ItselfConstructionLiteral
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;

        INFO("Itself construction from literal is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            auto literal = literalOf<ConcreteElement>();

            REQUIRE(!std::is_constructible_v<ConcreteSubject, decltype(literal)>);

        #ifdef ENABLE_COMPILE_ERROR
            ConcreteSubject subject{literal};
        #endif
        }
    }
};

template <class SubjectSpec>
void itselfConstructionLiteral()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ItselfConstructionLiteral,
          ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ItselfConstructionCompatible
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;

        INFO("Itself construction is valid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            auto literal = literalOf<ConcreteElement>();

            // INFO("Init itself of " + Utility::typeName<ConcreteSubject>()
            //        + "\n  by default")
            {
                if constexpr (std::is_default_constructible_v<ConcreteElement>)
                {
                    REQUIRE(std::is_constructible_v<ConcreteSubject,
                                                    Itself_Type<ConcreteSubjectSpec>>);

                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        ConcreteSubject subject{Itself<ConcreteSubjectSpec>});
                    O_T_CHECK_NOEXCEPT((ConcreteSubject{Itself<ConcreteSubjectSpec>}));

                    ConcreteElement subject_default{};
                    REQUIRE(isEqual(subject, subject_default));
                }
                else
                {
                    // NOTE: Type requirements in otn::base::token(otn::itself, ...)
                    // doesn't work with incomplete types.
                    // REQUIRE(!std::is_constructible_v<ConcreteSubject,
                    //                                  Itself_Type<ConcreteSubjectSpec>>);

                #ifdef ENABLE_COMPILE_ERROR
                    ConcreteSubject subject{Itself<ConcreteSubjectSpec>};
                #endif
                }
            }

            if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
            {
                // INFO("Init itself of " + Utility::typeName<Subject>()
                //        + "\n     from type " + Utility::typeName<ConcreteElement>()
                //        + "\n  by default")
                {
                    if constexpr (std::is_default_constructible_v<ConcreteElement>)
                    {
                        REQUIRE(std::is_constructible_v<Subject,
                                                        ItselfType_Type<ConcreteElement, SubjectSpec>>);

                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            Subject subject{ItselfType<ConcreteElement, SubjectSpec>});
                        O_T_CHECK_NOEXCEPT((Subject{ItselfType<ConcreteElement, SubjectSpec>}));

                        ConcreteElement subject_default{};
                        REQUIRE(isEqual(subject, subject_default));
                    }
                    else
                    {
                        REQUIRE(!std::is_constructible_v<Subject,
                                                         ItselfType_Type<ConcreteElement, SubjectSpec>>);

                    #ifdef ENABLE_COMPILE_ERROR
                        Subject subject{ItselfType<ConcreteElement, SubjectSpec>};
                    #endif
                    }
                }
            }

            // INFO("Init itself of " + Utility::typeName<ConcreteSubject>()
            //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
            {
                REQUIRE(std::is_constructible_v<ConcreteSubject,
                                                Itself_Type<ConcreteSubjectSpec>, decltype(literal)>);

                O_T_REQUIRE_CONTRACT_COMPLIANCE(
                    ConcreteSubject subject{Itself<ConcreteSubjectSpec>, literal});
                O_T_CHECK_NOEXCEPT((ConcreteSubject{Itself<ConcreteSubjectSpec>, literal}));

                REQUIRE(isEqual(subject, literal));
            }

            if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
            {
                // INFO("Init itself of " + Utility::typeName<Subject>()
                //        + "\n     from type " + Utility::typeName<ConcreteElement>()
                //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
                {
                    REQUIRE(std::is_constructible_v<Subject,
                                                    ItselfType_Type<ConcreteElement, SubjectSpec>, decltype(literal)>);

                    O_T_REQUIRE_CONTRACT_COMPLIANCE(
                        Subject subject{ItselfType<ConcreteElement, SubjectSpec>, literal});
                    O_T_CHECK_NOEXCEPT((Subject{ItselfType<ConcreteElement, SubjectSpec>, literal}));

                    REQUIRE(isEqual(subject, literal));
                }
            }
        }
    }
};

template <class SubjectSpec>
void itselfConstructionCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ItselfConstructionCompatible,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ItselfConstructionIsInvalid
{
    void operator()()
    {
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;

        INFO("Itself construction is invalid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nfrom " + Utility::typeName<SourceElement>())
        {
            auto literal = literalOf<ConcreteElement>();

            // INFO("Init itself of " + Utility::typeName<ConcreteSubject>()
            //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
            {
                REQUIRE(!std::is_constructible_v<ConcreteSubject,
                                                 Itself_Type<ConcreteSubjectSpec>, decltype(literal)>);

                #ifdef ENABLE_COMPILE_ERROR
                {
                    ConcreteSubject subject{Itself<ConcreteSubjectSpec>, literal};
                    REQUIRE(isEqual(subject, literal));
                }
                #endif
            }

            if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
            {
                // INFO("Init itself of " + Utility::typeName<Subject>()
                //        + "\n     from type " + Utility::typeName<ConcreteElement>()
                //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
                {
                    REQUIRE(!std::is_constructible_v<Subject,
                                                     ItselfType_Type<ConcreteElement, SubjectSpec>, decltype(literal)>);

                    #ifdef ENABLE_COMPILE_ERROR
                    {
                        Subject subject{ItselfType<ConcreteElement, SubjectSpec>, literal};
                        REQUIRE(isEqual(subject, literal));
                    }
                    #endif
                }
            }
        }
    }
};

template <class SubjectSpec>
void itselfConstructionIsInvalid()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ItselfConstructionIsInvalid,
          ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ItselfConstructionConstIncorrect
{
    void operator()()
    {
        if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
        {
            using SourceElement   = typename SourceSpec::Element;
            using ConcreteElement = Conform::ConcreteOf<SourceElement>;

            using Subject = Instance<SubjectSpec>;

            INFO("Itself construction is const incorrect"
                 "\n  of " + Utility::typeName<Subject>()
                 + "\nfrom " + Utility::typeName<SourceElement>())
            {
                auto literal = literalOf<ConcreteElement>();

                // INFO("Init itself of " + Utility::typeName<Subject>()
                //        + "\n     from type " + Utility::typeName<ConcreteElement>()
                //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
                {
                    REQUIRE(!std::is_constructible_v<Subject,
                                                     ItselfType_Type<ConcreteElement, SubjectSpec>, decltype(literal)>);

                #ifdef ENABLE_COMPILE_ERROR
                    Subject subject{ItselfType<ConcreteElement, SubjectSpec>, literal};
                #endif
                }
            }
        }
    }
};

template <class SubjectSpec>
void itselfConstructionConstIncorrect()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<ItselfConstructionConstIncorrect,
          ConstIncorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ItselfConstructionIncompatible
{
    void operator()()
    {
        if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
        {
            using SourceElement         = typename SourceSpec::Element;
            using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;

            using Subject = Instance<SubjectSpec>;

            auto literal = literalOf<ConcreteSourceElement>();

            INFO("Itself construction is incompatible"
                 "\nof itself of " + Utility::typeName<Subject>()
                 + "\n   from type " + Utility::typeName<ConcreteSourceElement>()
                 + "\nfrom literal " + Utility::typeName<decltype(literal)>())
            {
                REQUIRE(!std::is_constructible_v<Subject,
                                                 ItselfType_Type<ConcreteSourceElement, SubjectSpec>, decltype(literal)>);

            #ifdef ENABLE_COMPILE_ERROR
                Subject subject{ItselfType<ConcreteSourceElement, SubjectSpec>, literal};
            #endif
            }
        }
    }
};

template <class SubjectSpec>
void itselfConstructionIncompatible()
{
    using namespace Combo::Binary;
    Apply<ItselfConstructionIncompatible,
          Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

template <class SubjectSpec, class = void>
struct ItselfConstructionAbstract {};

template <class SubjectSpec>
struct ItselfConstructionAbstract<SubjectSpec,
                                  std::enable_if_t<!std::is_abstract_v<typename SubjectSpec::Element>>>
{ void operator()() {} };

template <class SubjectSpec>
struct ItselfConstructionAbstract<SubjectSpec,
                                  std::enable_if_t<std::is_abstract_v<typename SubjectSpec::Element>>>
{
    void operator()()
    {
        using SubjectElement = typename SubjectSpec::Element;

        using Subject = Instance<SubjectSpec>;

        INFO("Itself construction is abstract"
             "\n  of " + Utility::typeName<Subject>())
        {
            auto literal = literalOf<SubjectElement>();

            // INFO("Init itself of " + Utility::typeName<Subject>()
            //        + "\n    from literal " + Utility::typeName<decltype(literal)>())
            {
                // NOTE: Type requirements in otn::base::token(otn::itself, ...)
                // doesn't work with incomplete types.
                // REQUIRE(!std::is_constructible_v<Subject,
                //                                  Itself_Type<SubjectSpec>, decltype(literal)>);

            #ifdef ENABLE_COMPILE_ERROR
                Subject subject{Itself<SubjectSpec>, literal};
            #endif
            }

            if constexpr (Spec::IsItselfTypeConstructible<SubjectSpec>)
            {
                // INFO("Init itself of " + Utility::typeName<Subject>()
                //        + "\n     from type " + Utility::typeName<SubjectElement>()
                //        + "\n  from literal " + Utility::typeName<decltype(literal)>())
                {
                    REQUIRE(!std::is_constructible_v<Subject,
                                                     ItselfType_Type<SubjectElement, SubjectSpec>, decltype(literal)>);

                #ifdef ENABLE_COMPILE_ERROR
                    Subject subject{ItselfType<SubjectElement, SubjectSpec>, literal};
                #endif
                }
            }
        }
    }
};

template <class SubjectSpec>
void itselfConstructionAbstract()
{
    using namespace Combo::Unary;
    Apply<ItselfConstructionAbstract,
          SubjectSpec>()();
}

} // namespace

template <class SubjectSpec>
struct ItselfConstruction<SubjectSpec>
{
    void operator()()
    {
        if constexpr (Spec::IsItselfConstructible<SubjectSpec>)
        {
            itselfConstructionLiteral<SubjectSpec>();
            itselfConstructionCompatible<SubjectSpec>();
            itselfConstructionConstIncorrect<SubjectSpec>();
            itselfConstructionIncompatible<SubjectSpec>();
            itselfConstructionAbstract<SubjectSpec>();
        }
        else
        {
            itselfConstructionIsInvalid<SubjectSpec>();
        }
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
