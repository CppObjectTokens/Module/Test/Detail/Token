/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#ifdef TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS
#ifdef _MSC_VER
#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN \
    __pragma(warning( push )) \
    __pragma(warning( disable : 4996 ))

#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END \
    __pragma(warning( pop ))
#else // _MSC_VER
#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN \
    _Pragma("GCC diagnostic push") \
    _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")

#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END \
    _Pragma("GCC diagnostic pop")
#endif // _MSC_VER
#else
#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN
#define TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
#endif
