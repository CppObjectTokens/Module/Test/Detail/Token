/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Conform.h>
#include <Test/Token/Instance.h>
#include <Test/Token/Token/Trait.h>

#include <memory>
#include <type_traits>
#include <utility>

namespace Test
{

template <class Spec>
using Itself_Type = typename Bind::Itself_Type<typename Spec::Basis>;

template <class Spec>
inline constexpr Itself_Type<Spec> Itself = Bind::Itself<typename Spec::Basis>;

template <class Element, class Spec>
using ItselfType_Type =
    typename Bind::ItselfType_Type<Element, typename Spec::Basis>;

template <class Element, class Spec>
inline constexpr ItselfType_Type<Element, Spec> ItselfType =
    Bind::ItselfType<Element, typename Spec::Basis>;

template <class From, class To>
inline bool constexpr IsDerived =
    !std::is_same_v<From, To>
    && std::is_base_of_v<To, From>
    && std::is_convertible_v<std::remove_cv_t<From>*,
                             std::remove_cv_t<To>*>;

template <class Subject>
constexpr auto toValue(const Subject& subject) noexcept
  ->decltype(std::pointer_traits<Subject>::to_value(subject))
{
    return std::pointer_traits<Subject>::to_value(subject);
}

template <class Subject, class ... None>
constexpr decltype (auto) toValue(const Subject& subject, None ...) noexcept
{
    return *subject;
}

template <class Subject>
std::add_pointer_t<Trait::Element<Subject>> toAddress(const Subject& subject) noexcept
{
    if constexpr (Token::IsTrackableObserver<Subject>)
    {
        if constexpr (Token::IsLockable<Subject>)
        {
            if (subject.expired())
                return nullptr;
            else
                return toAddress(subject.lock());
        }
        else
        {
            using ProxySpec = Conform::ProxySpec<Trait::Spec<Subject>>;
            using Proxy     = Instance<ProxySpec>;
            Proxy proxy{subject};
            return toAddress(proxy);
        }
    }
    else
    {
        if constexpr (Token::IsStdSmart<Subject>)
            return subject.get();
        else
            return std::pointer_traits<Subject>::to_address(subject);
    }
}

template <class Subject>
auto toAddress(Subject* subject) noexcept
{
    return subject;
}

template <class Owner, class ConcreteElement, class ... Args>
inline
auto makeOwner(Args&& ... args)
{
    if constexpr (Trait::IsItselfTypeConstructible<Owner>)
        return Owner{Bind::ItselfType<ConcreteElement, Trait::Basis<Owner>>,
                     std::forward<Args>(args)...};
    else if constexpr (Trait::IsItselfConstructible<Owner>)
        return Owner{Bind::Itself<Trait::Basis<Owner>>,
                     std::forward<Args>(args)...};
    else
        return new ConcreteElement(std::forward<Args>(args)...);
}

template <class Spec, class Owner>
inline
decltype(auto) forwardOwner(Owner & owner) noexcept
{
    if constexpr (  Test::Spec::IsOwner<Spec>
                 && std::is_same_v<typename Spec::Ownership,
                                   Trait::Ownership<Owner>>)
        return owner;
    else
        return Instance<Spec>{owner};
}

template <class Subject, class Source>
inline
std::enable_if_t<Token::IsExplicitCopy<Subject, Source>, Subject>
forwardExplicit(Source&& source) noexcept(
    noexcept(Subject { std::forward<Source>(source) }))
{
    return Subject{std::forward<Source>(source)};
}

template <class Subject, class Source>
inline
std::enable_if_t<!Token::IsExplicitCopy<Subject, Source>, Source&&>
forwardExplicit(Source&& source) noexcept
{
    return std::forward<Source>(source);
}

template <class Subject>
inline std::enable_if_t<std::is_move_constructible_v<Subject>>
utilize(Subject&& subject) noexcept
{ [[maybe_unused]] Subject trash{std::move(subject)}; }

template <class Subject>
inline std::enable_if_t<!std::is_move_constructible_v<Subject>>
utilize(Subject&& subject) = delete;

template <class Subject>
inline void utilize(Subject& subject) = delete;

template <class HolderSpec, class SubjectSpec, class Raw>
void cleanup(Raw* raw) noexcept
{
    if constexpr (!(  Spec::IsOwner<HolderSpec>
                   || Spec::IsOwner<SubjectSpec>))
        delete raw;
}

} // namespace Test
