/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Basis.h>
#include <Test/Token/Ownership.h>
#include <Test/Token/Multiplicity.h>
#include <Test/Token/Lifetime.h>
#include <Test/Token/Spec/Generic.h>

#include <memory>
#include <type_traits>

namespace Test
{

namespace Bind
{

namespace Internal
{

template <class Entry>
struct Gateway
{
    static_assert(sizeof(Entry) == -1,
                  "Gateway is defined for the Entry");
};

namespace Trait
{

template <class Token, class = void>
struct Element
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Element is defined for the Token");
};

template <class Token, class = void>
struct Basis
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Basis is defined for the Token");
};

template <class Token, class = void>
struct Ownership
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Ownership is defined for the Token");
};

template <class Token, class = void>
struct Multiplicity
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Multiplicity is defined for the Token");
};

template <class Token, class = void>
struct Deleter
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Deleter is defined for the Token");
};

namespace Lifetime
{

template <class Token, class = void>
struct Role
{
    static_assert(sizeof(Token) == -1,
                  "Trait::Lifetime::Role is defined for the Token");
};

} // namespace Lifetime

template <class Token, class = void>
struct IsToken
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsToken is defined for the Token");
};

template <class Token, class = void>
struct IsOwner
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsOwner is defined for the Token");
};

template <class Token, class = void>
struct IsObserver
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsObserver is defined for the Token");
};

template <class Token, class = void>
struct IsOptional
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsOptional is defined for the Token");
};

template <class Token, class = void>
struct IsSingle
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsSingle is defined for the Token");
};

template <class Token, class = void>
struct IsTrackable
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsTrackable is defined for the Token");
};

template <class Token, class = void>
struct IsDereferenceable
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsDereferenceable is defined for the Token");
};

template <class Token, class = void>
struct IsDirectAccessible
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsDirectAccessible is defined for the Token");
};

template <class Token, class = void>
struct IsProxyAccessible
{
    static_assert(sizeof(Token) == -1,
                  "Trait::IsProxyAccessible is defined for the Token");
};

} // namespace Trait

template <class Basis, class = void>
struct Itself
{
    static_assert(sizeof(Basis) == -1,
                  "Itself is defined for the Basis");
};

template <class Element, class Basis, class = void>
struct ItselfType
{
    static_assert(sizeof(Basis) == -1,
                  "ItselfType is defined for the Basis");
};

template <class Element, class Basis, class Ownership, class Multiplicity,
          class = void>
struct Instance
{ using Type = void; };

namespace Rule
{

template <class Token>
struct GuessElement
{ using Type = void; };

template <class Token>
struct GuessElement<Token*>
{ using Type = Token; };

template <template <class Element, class ... Args> class Token,
          class Element, class ... Args>
struct GuessElement<Token<Element, Args...>>
{ using Type = Element; };

template <class Token, class Basis, class Ownership, class Multiplicity>
using IsInstanceOf = std::enable_if_t<
    std::is_same_v<Token,
                   typename Instance<typename GuessElement<Token>::Type,
                                     Basis,
                                     Ownership,
                                     Multiplicity>::Type>>;

template <class Token, class Basis, class Ownership, class Multiplicity>
using TokenSpec = Test::Spec::Generic<typename std::pointer_traits<Token>::element_type,
                                      Basis,
                                      Ownership,
                                      Multiplicity>;

template <class Token, class = void>
struct Spec : Test::Spec::Generic<Token,
                                  Test::Basis::Unknown,
                                  Test::Ownership::Unknown,
                                  Test::Multiplicity::Unknown> {};

template <class Basis, class Ownership, class = void>
struct Lifetime
{
    static_assert(sizeof(Basis) == -1,
                  "Lifetime is defined for the Basis");
};

template <class Ownership>
struct Lifetime<Test::Basis::Unknown, Ownership>
{
    static constexpr auto Role        = Test::Lifetime::Role::None;
    static constexpr bool IsTrackable = false;
};

template <class Basis, class Ownership, class = void>
inline constexpr bool IsLockable = false;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsExpirable = false;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsDereferenceable = true;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsDirectAccessible = IsDereferenceable<Basis, Ownership>;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsProxyAccessible = false;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsPointerConstructible = false;

template <class Basis, class Ownership, class = void>
inline constexpr bool IsItselfConstructible = false;

template <class Basis, class Ownership, class Multiplicity, class = void>
inline constexpr bool IsItselfTypeConstructible =
    IsItselfConstructible<Basis, Ownership>;

enum class ObjectState
{
    Unchanged,
    Unspecified,
    Empty
};

struct Disabled
{
    static constexpr bool IsEnabled        = false;
    static constexpr bool IsExplicit       = false;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Unchanged;

    using ExceptionType = void;
};

struct ImplicitCopy
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = false;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Unchanged;

    using ExceptionType = void;
};

struct ExplicitCopy
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = true;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Unchanged;

    using ExceptionType = void;
};

struct ImplicitMove
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = false;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Empty;

    using ExceptionType = void;
};

struct ExplicitMove
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = true;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Empty;

    using ExceptionType = void;
};

struct ImplicitMoveUnspecified
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = false;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Unspecified;

    using ExceptionType = void;
};

struct ExplicitMoveUnspecified
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = true;
    static constexpr bool IsRelocateObject = false;
    static constexpr auto SourcePostState  = ObjectState::Unspecified;

    using ExceptionType = void;
};

struct ImplicitMoveRelocateObject
{
    static constexpr bool IsEnabled        = true;
    static constexpr bool IsExplicit       = false;
    static constexpr bool IsRelocateObject = true;
    static constexpr auto SourcePostState  = ObjectState::Unspecified;

    using ExceptionType = void;
};

// Token<T>{token<Y>}
// Token<T> = token<Y>
template <class T_Basis, class T_Ownership, class T_Multiplicity,
          class Y_Basis, class Y_Ownership, class Y_Multiplicity>
struct CanCopy : Disabled {};

// Token<T>{std::move(token<Y>)}
// Token<T> = std::move(token<Y>)
template <class T_Basis, class T_Ownership, class T_Multiplicity,
          class Y_Basis, class Y_Ownership, class Y_Multiplicity>
struct CanMove : Disabled {};

template <class Basis, class Ownership, class = void>
inline constexpr bool IsOwnerBased = false;

template <class Basis, class Ownership, class Multiplicity,
          class Feature, class = void>
inline constexpr bool HasFeature = false;

} // namespace Rule

namespace Helper
{

template <class Basis, class Dummy = void>
struct Access
{
    static_assert(sizeof(Basis) == -1,
                  "Access is defined for the Basis");
};

template <class Basis, class Dummy = void>
struct Gain
{
    static_assert(sizeof(Basis) == -1,
                  "Gain is defined for the Basis");
};

} // namespace Helper

} // namespace Internal

namespace Gateway
{

template <class Entry>
using Inner = typename Internal::Gateway<Entry>::Inner;

template <class Entry>
using Outer = typename Internal::Gateway<Entry>::Outer;

template <class Entry>
inline constexpr auto InnerValue(Entry value)
{ return Internal::Gateway<Entry>::InnerValue(value); }

template <class Entry>
inline constexpr auto OuterValue(Entry value)
{ return Internal::Gateway<Entry>::OuterValue(value); }

} // namespace Gateway

namespace Trait
{

template <class Token>
using Element = typename Internal::Trait::Element<Token>::Type;

template <class Token>
using Basis = typename Internal::Trait::Basis<Token>::Type;

template <class Token>
using Ownership = typename Internal::Trait::Ownership<Token>::Type;

template <class Token>
using Multiplicity = typename Internal::Trait::Multiplicity<Token>::Type;

template <class Token>
using Deleter = typename Internal::Trait::Deleter<Token>::Type;

namespace Lifetime
{

template <class Token>
inline constexpr auto Role = Internal::Trait::Lifetime::Role<Token>::Value;

} // namespace Lifetime

template <class Token>
inline constexpr bool IsToken = Internal::Trait::IsToken<Token>::Value;

template <class Token>
inline constexpr bool IsOwner = Internal::Trait::IsOwner<Token>::Value;

template <class Token>
inline constexpr bool IsObserver = Internal::Trait::IsObserver<Token>::Value;

template <class Token>
inline constexpr bool IsOptional = Internal::Trait::IsOptional<Token>::Value;

template <class Token>
inline constexpr bool IsSingle = Internal::Trait::IsSingle<Token>::Value;

template <class Token>
inline constexpr bool IsTrackable = Internal::Trait::IsTrackable<Token>::Value;

template <class Token>
inline constexpr bool IsDereferenceable = Internal::Trait::IsDereferenceable<Token>::Value;

template <class Token>
inline constexpr bool IsDirectAccessible = Internal::Trait::IsDirectAccessible<Token>::Value;

template <class Token>
inline constexpr bool IsProxyAccessible = Internal::Trait::IsProxyAccessible<Token>::Value;

} // namespace Trait

template <class Basis>
using Itself_Type = typename Internal::Itself<Basis>::Type;

template <class Basis>
inline constexpr Itself_Type<Basis> Itself =
    Internal::Itself<Basis>::Value;

template <class Element, class Basis>
using ItselfType_Type = typename Internal::ItselfType<Element, Basis>::Type;

template <class Element, class Basis>
inline constexpr ItselfType_Type<Element, Basis> ItselfType =
    Internal::ItselfType<Element, Basis>::Value;

template <class Element, class Basis, class Ownership, class Multiplicity>
using Instance =
    typename Internal::Instance<Element, Basis, Ownership, Multiplicity>::Type;

namespace Rule
{

template <class Token>
using Spec = Internal::Rule::Spec<std::decay_t<Token>>;

template <class Spec>
using Lifetime = Internal::Rule::Lifetime<typename Spec::Basis,
                                          typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsLockable =
    Internal::Rule::IsLockable<typename Spec::Basis,
                               typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsExpirable =
    Internal::Rule::IsExpirable<typename Spec::Basis,
                                typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsDereferenceable =
    Internal::Rule::IsDereferenceable<typename Spec::Basis,
                                      typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsDirectAccessible =
    Internal::Rule::IsDirectAccessible<typename Spec::Basis,
                                       typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsProxyAccessible =
    Internal::Rule::IsProxyAccessible<typename Spec::Basis,
                                      typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsPointerConstructible =
    Internal::Rule::IsPointerConstructible<typename Spec::Basis,
                                           typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsItselfConstructible =
    Internal::Rule::IsItselfConstructible<typename Spec::Basis,
                                          typename Spec::Ownership>;

template <class Spec>
inline constexpr bool IsItselfTypeConstructible =
    Internal::Rule::IsItselfTypeConstructible<typename Spec::Basis,
                                              typename Spec::Ownership,
                                              typename Spec::Multiplicity>;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool CanCopy =
    Internal::Rule::CanCopy<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::IsEnabled;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool CanMove =
    Internal::Rule::CanMove<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::IsEnabled;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsExplicitCopy =
    Internal::Rule::CanCopy<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::IsExplicit;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsExplicitMove =
    Internal::Rule::CanMove<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::IsExplicit;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveEmpty =
    Internal::Rule::CanMove<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::SourcePostState
    == Internal::Rule::ObjectState::Empty;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveUnspecified =
    Internal::Rule::CanMove<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::SourcePostState
    == Internal::Rule::ObjectState::Unspecified;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveRelocateObject =
    Internal::Rule::CanMove<typename SubjectSpec::Basis,
                            typename SubjectSpec::Ownership,
                            typename SubjectSpec::Multiplicity,
                            typename SourceSpec::Basis,
                            typename SourceSpec::Ownership,
                            typename SourceSpec::Multiplicity>::IsRelocateObject;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveInvalidateObservers =
    IsMoveRelocateObject<SubjectSpec, SourceSpec>;

template <class SubjectSpec, class SourceSpec>
using CopyException =
    typename Internal::Rule::CanCopy<typename SubjectSpec::Basis,
                                     typename SubjectSpec::Ownership,
                                     typename SubjectSpec::Multiplicity,
                                     typename SourceSpec::Basis,
                                     typename SourceSpec::Ownership,
                                     typename SourceSpec::Multiplicity>::ExceptionType;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsCopyNoexcept =
    std::is_void_v<CopyException<SubjectSpec, SourceSpec>>;

template <class SubjectSpec, class SourceSpec>
using MoveException =
    typename Internal::Rule::CanMove<typename SubjectSpec::Basis,
                                     typename SubjectSpec::Ownership,
                                     typename SubjectSpec::Multiplicity,
                                     typename SourceSpec::Basis,
                                     typename SourceSpec::Ownership,
                                     typename SourceSpec::Multiplicity>::ExceptionType;

template <class SubjectSpec, class SourceSpec>
inline constexpr bool IsMoveNoexcept =
    std::is_void_v<MoveException<SubjectSpec, SourceSpec>>;

template <class Spec>
inline constexpr bool IsOwnerBased =
    Internal::Rule::IsOwnerBased<typename Spec::Basis,
                                 typename Spec::Ownership>;

template <class Basis, class Ownership, class Multiplicity, class Feature>
inline constexpr bool HasFeature =
    Internal::Rule::HasFeature<Basis, Ownership, Multiplicity, Feature>;

} // namespace Rule

namespace Helper
{

template <class Basis, class = void>
using Access = Internal::Helper::Access<Basis>;

template <class Basis, class = void>
using Gain = Internal::Helper::Gain<Basis>;

} // namespace Helper

} // namespace Bind

} // namespace Test
