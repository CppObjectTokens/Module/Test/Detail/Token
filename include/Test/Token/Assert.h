/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#if (!defined NDEBUG) && (defined TOKEN_TEST_ENABLE_ASSERT)
#define TOKEN_TEST_ASSERT
#endif

#if defined TOKEN_TEST_ASSERT
#include <sstream>

namespace Test
{

struct ContractViolation
{
    std::size_t      line_number{};
    std::string_view file_name;
    std::string_view function_name;
    std::string_view comment;
    std::string_view assertion_level;
};

class Assert
{
public:
    static Assert& instance() noexcept
    {
        static thread_local Assert m_instance;
        return m_instance;
    }

    bool aborted() const noexcept { return m_aborted; }

    bool check(bool condition, const ContractViolation& description) noexcept
    {
        if (!(m_aborted || condition))
        {
            m_aborted     = true;
            m_description = description;
        }

        return condition;
    }

    void reset() noexcept
    {
        m_aborted     = false;
        m_description = ContractViolation{};
    }

    const ContractViolation& description() const noexcept
    { return m_description; }

    std::string_view function_name() const noexcept
    { return description().function_name; }

    std::string_view comment() const noexcept
    { return description().comment; }

    std::string source_location() const noexcept
    {
        if (description().file_name.empty())
            return {};

        std::string buffer;
        buffer.reserve(256);

        std::ostringstream oss{std::move(buffer)};
        oss << description().file_name << ":" << description().line_number;

        return std::move(oss).str();
    }

private:
    bool m_aborted{};
    ContractViolation m_description;
};

} // namespace Test

#define O_T_REQUIRE_ABORT(...)                                                 \
    ::Test::Assert::instance().reset();                                        \
    __VA_ARGS__;                                                               \
    {                                                                          \
        INFO("because no abort was called where one was expected.");           \
        REQUIRE(::Test::Assert::instance().aborted());                         \
    }                                                                          \

#define O_T_REQUIRE_CONTRACT_COMPLIANCE(...)                                   \
    ::Test::Assert::instance().reset();                                        \
    __VA_ARGS__;                                                               \
    {                                                                          \
        INFO("due to a contract violation:\n"                                  \
             "comment : '" << ::Test::Assert::instance().comment() << "'\n"    \
             "function: " << ::Test::Assert::instance().function_name() << "\n" \
             "location: " << ::Test::Assert::instance().source_location());    \
        REQUIRE(!::Test::Assert::instance().aborted());                        \
    }                                                                          \

#define O_T_REQUIRE_CONTRACT_VIOLATION_AS(expr, comment_)                      \
    ::Test::Assert::instance().reset();                                        \
    expr;                                                                      \
    {                                                                          \
        INFO("because no contract was violated where one was expected:\n"      \
             "expected comment: '" << (comment_) << "'\n"                      \
             "contract comment: '" << ::Test::Assert::instance().comment() << "'"); \
        REQUIRE((  ::Test::Assert::instance().aborted()                        \
                && (std::string_view{comment_}                                 \
                    == ::Test::Assert::instance().comment())));                \
    }                                                                          \

#else // defined TOKEN_TEST_ASSERT
#define O_T_REQUIRE_ABORT(...) __VA_ARGS__;
#define O_T_REQUIRE_CONTRACT_COMPLIANCE(...) __VA_ARGS__;
#define O_T_REQUIRE_CONTRACT_VIOLATION_AS(expr, comment) expr;
#endif
