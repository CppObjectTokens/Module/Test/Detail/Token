/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

namespace Test
{

namespace Lifetime
{

enum class Role
{
    None,
    Observer,
    Owner
};

template <class L>
inline constexpr bool IsNone = L::Role == Role::None;
template <class L>
inline constexpr bool IsObserver = L::Role == Role::Observer;
template <class L>
inline constexpr bool IsOwner = L::Role == Role::Owner;

template <class L>
inline constexpr bool IsTrackable = L::IsTrackable;

template <class L>
inline constexpr bool IsToken = !IsNone<L>;

template <class L>
inline constexpr bool IsUntrackableObserver = IsObserver<L>&& !IsTrackable<L>;
template <class L>
inline constexpr bool IsTrackableObserver = IsObserver<L>&& IsTrackable<L>;
template <class L>
inline constexpr bool IsUntrackableOwner = IsOwner<L>&& !IsTrackable<L>;
template <class L>
inline constexpr bool IsTrackableOwner = IsOwner<L>&& IsTrackable<L>;

} // namespace Lifetime

} // namespace Test
