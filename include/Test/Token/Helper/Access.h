/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class = void>
struct AccessHelper
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class Subject, class Owner>
#ifdef _MSC_VER
__declspec(noinline)
#else
__attribute__((noinline))
#endif
Subject makeTemporary(Owner&& owner)
{
    #ifdef _MSC_VER
    #else
    asm ("");
    #endif

    if constexpr (Token::IsOwner<Subject>)
        return Subject{std::forward<Owner>(owner)};
    else
        return Subject{std::as_const(owner)};
}

template <class Element, class Literal>
bool accessTemporary(Element& element, const Literal& literal)
{
    return element == literal;
}

template <class Tokens, class Literals, std::size_t ... I>
bool accessTuple(Tokens&& tokens, Literals&& literals,
                 std::index_sequence<I...>)
{
    using std::get;
    return((*get<I>(tokens) == get<I>(literals)) && ...);
}

template <class Tokens, class Literals>
bool accessTuple(Tokens&& tokens, Literals&& literals)
{
    return accessTuple(std::forward<Tokens>(tokens),
                       std::forward<Literals>(literals),
                       std::make_index_sequence<std::tuple_size_v<std::decay_t<Tokens>>>{});
}

template <class SubjectSpec, class SourceSpec, class = void>
struct AccessCompatible
{
    template <class ... Tokens>
    static auto access(Tokens&& ... tokens)
    {
        return Helper::Access<typename SubjectSpec::Basis>::access(
            std::forward<Tokens>(tokens)...);
    }

    template <class ... Tokens>
    [[nodiscard]] static
    auto gain(Tokens&& ... tokens)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Tokens>(tokens)...);
    }

    void gain() = delete;

    void operator()()
    {
        using SubjectElement  = typename SubjectSpec::Element;
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SourceElement>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;
        using ConcreteSubjectSpec = Spec::ReplaceElement<SubjectSpec, ConcreteElement>;
        using ConcreteOwnerSpec   = Spec::ReplaceElement<OwnerSpec, ConcreteElement>;

        using Subject         = Instance<SubjectSpec>;
        using Owner           = Instance<OwnerSpec>;
        using ConcreteSubject = Instance<ConcreteSubjectSpec>;
        using ConcreteOwner   = Instance<ConcreteOwnerSpec>;

        INFO("Access is valid"
             "\nthe " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SourceElement*>() + "}")
        {
            auto literal = literalOf<ConcreteElement>();

            // INFO("Access the value " + Utility::typeName<Subject>()
            //        + "\n            with " + Utility::typeName<SubjectElement>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                const SourceElement* raw = toAddress(owner);

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                // Classic 'if-else' access
                if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                {
                    if (subject)
                    {
                        REQUIRE(isPointTo(*subject, raw));
                        REQUIRE(*subject == literal);
                    }

                    if (std::as_const(subject))
                    {
                        auto& subject_value = *subject;
                        REQUIRE(isPointTo(subject_value, raw));
                        REQUIRE(subject_value == literal);
                    }
                    else
                    {
                        REQUIRE(false);
                    }
                }
                else if constexpr (Spec::IsLockable<SubjectSpec>)
                {
                    if (auto subject_loc = subject.lock())
                    {
                        REQUIRE(isPointTo(*subject_loc, raw));
                        REQUIRE(*subject_loc == literal);
                    }

                    if (auto subject_loc = std::as_const(subject).lock())
                    {
                        auto& subject_value = *subject_loc;
                        REQUIRE(isPointTo(subject_value, raw));
                        REQUIRE(subject_value == literal);
                    }
                    else
                    {
                        REQUIRE(false);
                    }
                }

                // Unified 'if-else' access
                if (auto subject_loc = gain(subject))
                {
                    REQUIRE(isPointTo(*subject_loc, raw));
                    REQUIRE(*subject_loc == literal);
                }

                if (auto subject_loc = gain(std::as_const(subject)))
                {
                    auto& subject_value = *subject_loc;
                    REQUIRE(isPointTo(subject_value, raw));
                    REQUIRE(subject_value == literal);
                }
                else
                {
                    REQUIRE(false);
                }

                if (auto raw_loc = gain(raw))
                {
                    REQUIRE(isPointTo(*raw_loc, raw));
                    REQUIRE(*raw_loc == literal);
                }

                if (auto subject_loc = gain(subject))
                {
                    auto& subject_by_value = *subject_loc;
                    auto&& [subject_by_token] = subject_loc;
                    const auto& subject_by_const_value = *subject_loc;
                    const auto& [subject_by_const_token] = subject_loc;

                    REQUIRE(std::is_const_v<std::remove_reference_t<decltype (subject_by_value)>> == std::is_const_v<SubjectElement>);
                    REQUIRE(!std::is_const_v<std::remove_reference_t<decltype (subject_by_token)>>);
                    REQUIRE(std::is_const_v<std::remove_reference_t<decltype (subject_by_const_value)>>);
                    REQUIRE(std::is_const_v<std::remove_reference_t<decltype (subject_by_const_token)>>);

                    REQUIRE(isPointTo(subject_by_value, raw));
                    REQUIRE(isPointTo(*subject_by_token, raw));
                    REQUIRE(isPointTo(subject_by_const_value, raw));
                    REQUIRE(isPointTo(*subject_by_const_token, raw));

                    if constexpr (std::is_same_v<SubjectElement, int>)
                    {
                        ++subject_by_value;
                        #ifdef ENABLE_COMPILE_ERROR
                        ++subject_by_const_value;
                        #else
                        ++subject_by_value;
                        #endif

                        REQUIRE(*raw == literal + 2);

                        --*subject_by_token;
                        --*subject_by_const_token;
                    }
                }

                if (auto subject_loc = gain(subject))
                {
                    using std::get;

                    auto& subject_by_token = *get<0>(subject_loc);
                    auto& subject_by_value = *subject_loc;

                    REQUIRE(isPointTo(subject_by_token, raw));
                    REQUIRE(isPointTo(subject_by_value, raw));

                    if constexpr (std::is_same_v<SubjectElement, int>)
                    {
                        ++subject_by_token;
                        REQUIRE(*raw == literal + 1);
                        --subject_by_value;
                    }

                    REQUIRE(*get<0>(subject_loc) == literal);
                    REQUIRE(*subject_loc == literal);
                }

                REQUIRE(accessTuple(gain(subject), std::forward_as_tuple(literal)));

                #ifdef ENABLE_COMPILE_ERROR
                gain();        // no sense
                gain(subject); // ignoring of the [[nodiscard]]

                auto subject_loc = gain(subject);
                static_cast<bool>(subject_loc); // ignoring of the [[nodiscard]]

                //
                if (gain(subject))                          // no sense
                    auto& subject_direct = *gain(subject);  // the subject can be invalid here

                // using of the temporary
                {
                    auto& subject = *makeOwner<Owner, ConcreteElement>(literal);
                    REQUIRE(subject == literal);
                }
                {
                    auto& subject = *gain(makeOwner<Owner, ConcreteElement>(literal));
                    REQUIRE(subject == literal);
                }

                using std::get;

                // using of the temporary
                {
                    auto& subject = get<0>(gain(makeOwner<Owner, ConcreteElement>(literal)));
                    REQUIRE(isEqual(subject, literal));
                }
                {
                    auto& value = *get<0>(gain(makeOwner<Owner, ConcreteElement>(literal)));
                    REQUIRE(value == literal);
                }
                #endif

                // Lambda access
                access(subject, [&](auto& subject)
                        {
                            REQUIRE(isPointTo(subject, raw));
                            REQUIRE(subject == literal);
                        });

                access(std::as_const(subject), [&](auto& subject)
                        {
                            REQUIRE(isPointTo(subject, raw));
                            REQUIRE(subject == literal);
                        }, []
                        {
                            REQUIRE(false);
                        });

                access(raw, [&](auto& raw_value)
                        {
                            REQUIRE(isPointTo(raw_value, raw));
                            REQUIRE(raw_value == literal);
                        });

                #ifdef ENABLE_COMPILE_ERROR
                access();
                access(subject);
                #endif

                if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                {
                    // 'Range-based for' access
                    for (auto& subject : subject)
                    {
                        REQUIRE(isPointTo(*subject, raw));
                        REQUIRE(*subject == literal);
                    }

                    for (auto& subject : std::as_const(subject))
                    {
                        REQUIRE(isPointTo(*subject, raw));
                        REQUIRE(*subject == literal);
                    }

                    for (auto& subject : gain(subject))
                    {
                        REQUIRE(isPointTo(*subject, raw));
                        REQUIRE(*subject == literal);
                    }
                }

                O_T_CHECK_NOEXCEPT(gain(subject));
                O_T_CHECK_NOEXCEPT(gain(std::as_const(subject)));
                O_T_CHECK_NOEXCEPT(begin(subject));
                O_T_CHECK_NOEXCEPT(begin(std::as_const(subject)));
                O_T_CHECK_NOEXCEPT(end(subject));
                O_T_CHECK_NOEXCEPT(end(std::as_const(subject)));

                cleanup<OwnerSpec, SubjectSpec>(raw);
            }

            if constexpr (Spec::IsElementMoveConstructible<SubjectSpec, SubjectSpec>)
            {
                // INFO("Access the temporary " + Utility::typeName<Subject>()
                //     + "\n           with      " + Utility::typeName<SubjectElement>())
                {
                    // Classic 'if-else' access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                        {
                            if (const auto& subject = makeTemporary<Subject>(std::move(owner)))
                            {
                                auto& subject_value = *subject;
                                REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject_value, raw));
                                REQUIRE(subject_value == literal);
                            }
                            else
                            {
                                REQUIRE(false);
                            }
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                        {
                            REQUIRE(accessTemporary(*makeTemporary<Subject>(std::move(owner)), literal));
                            REQUIRE(std::is_const_v<std::remove_reference_t<decltype (*std::declval<Subject &&>())>>
                                    == std::is_const_v<SubjectElement>);
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // Unified 'if-else' access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        if (auto subject_loc = gain(makeTemporary<Subject>(std::move(owner))))
                        {
                            auto& subject_value = *subject_loc;
                            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject_value, raw));
                            REQUIRE(subject_value == literal);
                        }
                        else
                        {
                            REQUIRE(false);
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        REQUIRE(accessTemporary(*gain(makeTemporary<Subject>(std::move(owner))), literal));
                        REQUIRE(std::is_const_v<std::remove_reference_t<decltype (*gain(std::declval<Subject &&>()))>>
                                == std::is_const_v<SubjectElement>);

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        using std::get;
                        REQUIRE(accessTemporary(*get<0>(gain(makeTemporary<Subject>(std::move(owner)))), literal));
                        REQUIRE(!std::is_const_v<std::remove_reference_t<decltype (get<0>(gain(std::declval<Subject &&>())))>>);
                        REQUIRE(std::is_const_v<std::remove_reference_t<decltype (*get<0>(gain(std::declval<Subject &&>())))>>
                                == std::is_const_v<SubjectElement>);

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // Lambda access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        access(makeTemporary<Subject>(std::move(owner)), [&](auto& subject)
                                {
                                    REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject, raw));
                                    REQUIRE(subject == literal);
                                }, []
                                {
                                    REQUIRE(false);
                                });

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // 'Range-based for' access
                    if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        for (auto& subject : makeTemporary<Subject>(std::move(owner)))
                        {
                            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(*subject, raw));
                            REQUIRE(*subject == literal);
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }
                }

                // INFO("Access the moved " + Utility::typeName<Subject>()
                //     + "\n           with  " + Utility::typeName<SubjectElement>())
                {
                    // Classic 'if-else' access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                        {
                            if (auto subject = std::move(owner))
                            {
                                auto& subject_value = *subject;
                                REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject_value, raw));
                                REQUIRE(subject_value == literal);
                            }
                            else
                            {
                                REQUIRE(false);
                            }

                            REQUIRE(isMoved(owner));
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // Unified 'if-else' gain access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        if (auto subject_loc = gain(std::move(owner)))
                        {
                            auto& subject_value = *subject_loc;
                            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject_value, raw));
                            REQUIRE(subject_value == literal);
                        }
                        else
                        {
                            REQUIRE(false);
                        }

                        REQUIRE(isMoved(owner));

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // Lambda access
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        access(std::move(owner), [&](auto& subject)
                                {
                                    REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(subject, raw));
                                    REQUIRE(subject == literal);
                                }, []
                                {
                                    REQUIRE(false);
                                });

                        REQUIRE(isMoved(owner));

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }

                    // 'Range-based for' access
                    if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                    {
                        Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                        const SourceElement* raw = toAddress(owner);

                        for (auto& subject : makeTemporary<Subject>(std::move(owner)))
                        {
                            REQUIRE(isObserverIntegrity<SubjectSpec, SourceSpec>(*subject, raw));
                            REQUIRE(*subject == literal);
                        }

                        cleanup<OwnerSpec, SubjectSpec>(raw);
                    }
                }

                // INFO("Access the moved-from " + Utility::typeName<Subject>()
                //        + "\n           with       " + Utility::typeName<SubjectElement>())
                {
                    Owner owner{makeOwner<Owner, ConcreteElement>(literal)};
                    const SourceElement* raw = toAddress(owner);

                    auto&& subject = forwardOwner<SubjectSpec>(owner);

                    utilize(std::move(subject));

                #ifdef TOKEN_TEST_ASSERT
                    if constexpr (  Spec::IsMoveEmpty<SubjectSpec, SubjectSpec>
                                 && Spec::HasFeature<SubjectSpec,
                                                     Feature::ContractViolation::Abort>)
                    {
                        // Classic 'if-else' access
                        if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                        {
                            const int answer = 42;
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                [[maybe_unused]] const SubjectElement& view = *subject,
                                "access_to_valued_referrer()");
                        }
                        else if constexpr (Spec::IsLockable<SubjectSpec>)
                        {
                            REQUIRE(!subject.lock());
                        }

                        // Unified 'if-else' access
                        {
                            auto subject_loc = gain(subject);
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                [[maybe_unused]] const SubjectElement& view = *subject_loc,
                                "access_to_valued_referrer()");
                        }

                        if constexpr (Spec::IsSingle<SubjectSpec>)
                        {
                            // Lambda access
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                access(subject, [&](auto&) {}),
                                "access_to_valued_referrer()");

                            // 'Range-based for' access
                            O_T_REQUIRE_CONTRACT_VIOLATION_AS(
                                {for (auto& subject : subject) auto& subject_value = *subject; },
                                "access_to_valued_referrer()");
                        }
                    }
                    else
                #endif
                    {
                        // Classic 'if-else' access
                        if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                        {
                            const int answer = 42;
                            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                                [[maybe_unused]] volatile const SubjectElement& view = *subject);
                        }
                        else if constexpr (Spec::IsLockable<SubjectSpec>)
                        {
                            REQUIRE(!subject.lock());
                        }

                        // Unified 'if-else' access
                        {
                            auto subject_loc = gain(subject);
                            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                                [[maybe_unused]] volatile const SubjectElement& view = *subject_loc);
                        }

                        // Lambda access
                        O_T_REQUIRE_CONTRACT_COMPLIANCE(
                            access(subject, [&](auto&) {}));

                        // 'Range-based for' access
                        if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                        {
                            const int answer = 42;
                            O_T_REQUIRE_CONTRACT_COMPLIANCE(
                                for (auto& subject : subject)
                                [[maybe_unused]] volatile const SubjectElement& view = *subject; );
                        }
                    }

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }
            }

            if constexpr (IsDerived<ConcreteElement, SourceElement>)
            {
                // INFO("Access the derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n      with    base " + Utility::typeName<SubjectElement>())
                {
                    ConcreteOwner         owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    const SubjectElement* raw = toAddress(owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    // Classic 'if-else' access
                    if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                    {
                        if (subject)
                        {
                            REQUIRE(isPointTo(*subject, raw));
                            REQUIRE(*subject == literal);
                        }
                    }
                    else if constexpr (Spec::IsLockable<SubjectSpec>)
                    {
                        if (auto subject_loc = subject.lock())
                        {
                            REQUIRE(isPointTo(*subject_loc, raw));
                            REQUIRE(*subject_loc == literal);
                        }
                    }

                    // Unified 'if-else' access
                    if (auto subject_loc = gain(subject))
                    {
                        REQUIRE(isPointTo(*subject_loc, raw));
                        REQUIRE(*subject_loc == literal);
                    }

                    // Lambda access
                    access(subject, [&](auto& subject)
                            {
                                REQUIRE(isPointTo(subject, raw));
                                REQUIRE(subject == literal);
                            });

                    if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                    {
                        // 'Range-based for' access
                        for (auto& subject : subject)
                        {
                            REQUIRE(isPointTo(*subject, raw));
                            REQUIRE(*subject == literal);
                        }
                    }

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }

                // INFO("Access the derived " + Utility::typeName<ConcreteSubject>()
                //        + "\n      with derived " + Utility::typeName<ConcreteElement>())
                {
                    ConcreteOwner owner{makeOwner<ConcreteOwner, ConcreteElement>(literal)};
                    const ConcreteElement* raw = toAddress(owner);

                    auto&& subject = forwardOwner<ConcreteSubjectSpec>(owner);

                    // Classic 'if-else' access
                    if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                    {
                        if (subject)
                        {
                            REQUIRE(isPointTo(*subject, raw));
                            REQUIRE(*subject == literal);
                        }
                    }
                    else if constexpr (Spec::IsLockable<SubjectSpec>)
                    {
                        if (auto subject_loc = subject.lock())
                        {
                            REQUIRE(isPointTo(*subject_loc, raw));
                            REQUIRE(*subject_loc == literal);
                        }
                    }

                    // Unified 'if-else' access
                    if (auto subject_loc = gain(subject))
                    {
                        REQUIRE(isPointTo(*subject_loc, raw));
                        REQUIRE(*subject_loc == literal);
                    }

                    // Lambda access
                    access(subject, [&](auto& subject)
                            {
                                REQUIRE(isPointTo(subject, raw));
                                REQUIRE(subject == literal);
                            });

                    if constexpr (Spec::HasFeature<SubjectSpec, Feature::IsRange>)
                    {
                        // 'Range-based for' access
                        for (auto& subject : subject)
                        {
                            REQUIRE(isPointTo(*subject, raw));
                            REQUIRE(*subject == literal);
                        }
                    }

                    cleanup<OwnerSpec, SubjectSpec>(raw);
                }
            }
        }
    }
};

template <class SubjectSpec>
void accessCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<AccessCompatible, ConstCorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class = void>
struct AccessSeveral
{
    template <class ... Tokens>
    static auto access(Tokens&& ... tokens)
    {
        return Helper::Access<typename SubjectSpec::Basis>::access(
            std::forward<Tokens>(tokens)...);
    }

    template <class ... Tokens>
    [[nodiscard]] static
    auto gain(Tokens&& ... tokens)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Tokens>(tokens)...);
    }

    static void gain() = delete;

    void operator()()
    {
        using SubjectSpec1 = SubjectSpec;
        using SubjectSpec2 = SubjectSpec;
        using SubjectSpec3 = SubjectSpec;

        using OwnerSpec1 = Conform::OwnerSpec<SubjectSpec1>;
        using OwnerSpec2 = Conform::OwnerSpec<SubjectSpec2>;
        using OwnerSpec3 = Conform::OwnerSpec<SubjectSpec3>;

        using SubjectElement1  = typename SubjectSpec1::Element;
        using SubjectElement2  = typename SubjectSpec2::Element;
        using SubjectElement3  = typename SubjectSpec3::Element;
        using ConcreteElement1 = Conform::ConcreteOf<SubjectElement1>;
        using ConcreteElement2 = Conform::ConcreteOf<SubjectElement2>;
        using ConcreteElement3 = Conform::ConcreteOf<SubjectElement3>;

        using Subject1 = Instance<SubjectSpec1>;
        using Subject2 = Instance<SubjectSpec2>;
        using Subject3 = Instance<SubjectSpec3>;
        using Owner1   = Instance<OwnerSpec1>;
        using Owner2   = Instance<OwnerSpec2>;
        using Owner3   = Instance<OwnerSpec3>;

        INFO("Access a several is valid"
             "\nthe " + Utility::typeName<Subject1>()
             + "{" + Utility::typeName<SubjectElement1*>() + "}"
             + "\nthe " + Utility::typeName<Subject2>()
             + "{" + Utility::typeName<SubjectElement2*>() + "}")
        {
            auto literal_1 = literalOf<ConcreteElement1>();
            auto literal_2 = literalOf<ConcreteElement2>();
            auto literal_3 = literalOf<ConcreteElement3>();

            // INFO("Access the value " + Utility::typeName<Subject1>()
            //        + "\n            with " + Utility::typeName<SubjectElement1>()
            //        + "\n       the value " + Utility::typeName<Subject2>()
            //        + "\n            with " + Utility::typeName<SubjectElement2>())
            {
                Owner1 owner_1{makeOwner<Owner1, ConcreteElement1>(literal_1)};
                Owner2 owner_2{makeOwner<Owner2, ConcreteElement2>(literal_2)};
                Owner1 owner_3{makeOwner<Owner3, ConcreteElement3>(literal_3)};
                const SubjectElement1* raw_1 = toAddress(owner_1);
                const SubjectElement1* raw_2 = toAddress(owner_2);
                const SubjectElement1* raw_3 = toAddress(owner_3);

                auto&& subject_1 = forwardOwner<SubjectSpec1>(owner_1);
                auto&& subject_2 = forwardOwner<SubjectSpec2>(owner_2);
                auto&& subject_3 = forwardOwner<SubjectSpec3>(owner_3);

                // Classic 'if-else' access
                if constexpr (Spec::IsDereferenceable<SubjectSpec1>)
                {
                    if (subject_1 && subject_2)
                    {
                        REQUIRE(isPointTo(*subject_1, raw_1));
                        REQUIRE(*subject_1 == literal_1);
                        REQUIRE(isPointTo(*subject_2, raw_2));
                        REQUIRE(*subject_2 == literal_2);
                    }

                    if (std::as_const(subject_1) && std::as_const(subject_2))
                    {
                        auto& subject_1_value = *subject_1;
                        auto& subject_2_value = *subject_2;

                        REQUIRE(isPointTo(subject_1_value, raw_1));
                        REQUIRE(subject_1_value == literal_1);
                        REQUIRE(isPointTo(subject_2_value, raw_2));
                        REQUIRE(subject_2_value == literal_2);
                    }
                    else
                    {
                        REQUIRE(false);
                    }
                }
                else if constexpr (Spec::IsLockable<SubjectSpec1>)
                {
                    auto subject_1_loc = subject_1.lock();
                    auto subject_2_loc = subject_2.lock();
                    if (subject_1_loc && subject_2_loc)
                    {
                        REQUIRE(isPointTo(*subject_1_loc, raw_1));
                        REQUIRE(*subject_1_loc == literal_1);
                        REQUIRE(isPointTo(*subject_2_loc, raw_2));
                        REQUIRE(*subject_2_loc == literal_2);
                    }
                    else
                    {
                        REQUIRE(false);
                    }
                }

                // Unified 'if-else' access
                {
                    if (auto locs = gain(subject_1, subject_2))
                    {
                        using std::get;

                        // Token access
                        {
                            auto& subject_1 = get<0>(locs);
                            auto& subject_2 = get<1>(locs);

                            REQUIRE(isPointTo(*subject_1, raw_1));
                            REQUIRE(*subject_1 == literal_1);
                            REQUIRE(isPointTo(*subject_2, raw_2));
                            REQUIRE(*subject_2 == literal_2);

                            if constexpr (std::is_same_v<SubjectElement1, int>)
                            {
                                ++*subject_1;
                                REQUIRE(*raw_1 == literal_1 + 1);
                                --*subject_1;
                            }
                        }

                        // Value access
                        {
                            auto& subject_1 = get<0>(*locs);
                            auto& subject_2 = get<1>(*locs);

                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);

                            if constexpr (std::is_same_v<SubjectElement1, int>)
                            {
                                ++subject_1;
                                REQUIRE(*raw_1 == literal_1 + 1);
                                --subject_1;
                            }
                        }

                    #ifdef ENABLE_COMPILE_ERROR
                        REQUIRE(isPointTo(*locs, raw_1));
                    #endif
                    }

                    auto locs = gain(subject_1, subject_2, std::as_const(subject_3));
                    if (locs)
                    {
                        // Token access
                        {
                            auto& [subject_1, subject_2, subject_3] = locs;

                            REQUIRE(isPointTo(*subject_1, raw_1));
                            REQUIRE(*subject_1 == literal_1);
                            REQUIRE(isPointTo(*subject_2, raw_2));
                            REQUIRE(*subject_2 == literal_2);
                            REQUIRE(isPointTo(*subject_3, raw_3));
                            REQUIRE(*subject_3 == literal_3);
                        }

                        // Value access
                        {
                            auto& [subject_1, subject_2, subject_3] = *locs;

                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                            REQUIRE(isPointTo(subject_3, raw_3));
                            REQUIRE(subject_3 == literal_3);
                        }
                    }
                    else
                    {
                        REQUIRE(false);
                    }

                    if (auto locs = gain(subject_1, subject_2, raw_3))
                    {
                        // Token access
                        {
                            const auto& [subject_1, subject_2, subject_3] = locs;

                            REQUIRE(isPointTo(*subject_1, raw_1));
                            REQUIRE(*subject_1 == literal_1);
                            REQUIRE(isPointTo(*subject_2, raw_2));
                            REQUIRE(*subject_2 == literal_2);
                            REQUIRE(isPointTo(*subject_3, raw_3));
                            REQUIRE(*subject_3 == literal_3);
                        }

                        // Value access
                        {
                            const auto& [subject_1, subject_2, subject_3] = *locs;

                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                            REQUIRE(isPointTo(subject_3, raw_3));
                            REQUIRE(subject_3 == literal_3);
                        }
                    }
                }

                REQUIRE(accessTuple(gain(subject_1, subject_2, raw_3),
                                    std::forward_as_tuple(literal_1, literal_2, literal_3)));

                // Lambda access
                access(subject_1, subject_2,
                       [&](auto& subject_1, auto& subject_2)
                        {
                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                        });

                access(std::as_const(subject_1), std::as_const(subject_2),
                       [&](auto& subject_1, auto& subject_2)
                        {
                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                        }, []
                        {
                            REQUIRE(false);
                        });

                access(subject_1, subject_2, subject_3,
                       [&](auto& subject_1, auto& subject_2, auto& subject_3)
                        {
                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                            REQUIRE(isPointTo(subject_3, raw_3));
                            REQUIRE(subject_3 == literal_3);
                        });

                access(std::as_const(subject_1),
                       std::as_const(subject_2),
                       std::as_const(subject_3),
                       [&](auto& subject_1, auto& subject_2, auto& subject_3)
                        {
                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                            REQUIRE(isPointTo(subject_3, raw_3));
                            REQUIRE(subject_3 == literal_3);
                        }, []
                        {
                            REQUIRE(false);
                        });

                access(subject_1, subject_2, raw_3,
                       [&](auto& subject_1, auto& subject_2, auto& subject_3)
                        {
                            REQUIRE(isPointTo(subject_1, raw_1));
                            REQUIRE(subject_1 == literal_1);
                            REQUIRE(isPointTo(subject_2, raw_2));
                            REQUIRE(subject_2 == literal_2);
                            REQUIRE(isPointTo(subject_3, raw_3));
                            REQUIRE(subject_3 == literal_3);
                        });

                // 'Range-based for' access
                {
                    int iter_count = 0;
                    for (auto& [subject_1, subject_2]
                         : gain(subject_1, subject_2))
                    {
                        ++iter_count;
                        REQUIRE(isPointTo(*subject_1, raw_1));
                        REQUIRE(*subject_1 == literal_1);
                        REQUIRE(isPointTo(*subject_2, raw_2));
                        REQUIRE(*subject_2 == literal_2);
                    }
                    REQUIRE(iter_count == 1);
                }

                {
                    int iter_count = 0;
                    for (auto& [subject_1, subject_2, subject_3]
                         : gain(subject_1, subject_2, subject_3))
                    {
                        ++iter_count;
                        REQUIRE(isPointTo(*subject_1, raw_1));
                        REQUIRE(*subject_1 == literal_1);
                        REQUIRE(isPointTo(*subject_2, raw_2));
                        REQUIRE(*subject_2 == literal_2);
                        REQUIRE(isPointTo(*subject_3, raw_3));
                        REQUIRE(*subject_3 == literal_3);
                    }
                    REQUIRE(iter_count == 1);
                }

                cleanup<OwnerSpec3, SubjectSpec3>(raw_3);
                raw_3 = nullptr;
                int iter_count = 0;
                for ([[maybe_unused]] auto& [subject_1, subject_2, subject_3]
                     : gain(subject_1, subject_2, raw_3))
                { ++iter_count; }
                REQUIRE(iter_count == 0);

                if constexpr (Spec::IsTolerateMovedFrom<SubjectSpec2, SubjectSpec2>)
                {
                    utilize(std::move(subject_2));

                    iter_count = 0;
                    if constexpr (  Spec::IsSingle<SubjectSpec2>
                                 && Spec::IsDereferenceable<SubjectSpec2>)
                    {
                        for (auto& [subject_1, subject_2]
                             : gain(subject_1, subject_2))
                        {
                            ++iter_count;
                            REQUIRE(isPointTo(*subject_1, raw_1));
                            REQUIRE(*subject_1 == literal_1);
                        }
                        REQUIRE(iter_count == 1);
                    }
                    else
                    {
                        for ([[maybe_unused]] auto& [subject_1, subject_2]
                             : gain(subject_1, subject_2))
                        { ++iter_count; }
                        REQUIRE(iter_count == 0);
                    }
                }

                cleanup<OwnerSpec1, SubjectSpec1>(raw_1);
                cleanup<OwnerSpec2, SubjectSpec2>(raw_2);
            }
        }
    }
};

template <class SubjectSpec>
void accessSeveral()
{
    AccessSeveral<SubjectSpec>{} ();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct AccessConstIncorrect
{
    template <class ... Tokens>
    static auto access(Tokens&& ... tokens)
    {
        return Helper::Access<typename SubjectSpec::Basis>::access(
            std::forward<Tokens>(tokens)...);
    }

    template <class Token>
    [[nodiscard]] static
    decltype(auto) gain(Token && token)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Token>(token));
    }

    template <class ... Tokens>
    [[nodiscard]] static
    auto gain(Tokens&& ... tokens)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Tokens>(tokens)...);
    }

    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectElement = typename SubjectSpec::Element;
        using SourceElement  = typename SourceSpec::Element;

        using SubjectSourceSpec = Spec::ReplaceElement<SubjectSpec, SourceElement>;
        using OwnerSpec         = Conform::OwnerSpec<SubjectSourceSpec>;

        using Subject = Instance<SubjectSourceSpec>;
        using Owner   = Instance<OwnerSpec>;

        INFO("Access is const incorrect"
             "\nthe " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SourceElement*>() + "}")
        {
            // INFO("Access the " + Utility::typeName<Subject>()
            //        + "\n      with " + Utility::typeName<SubjectElement>())
            {
                using ConcreteElement = Conform::ConcreteOf<SourceElement>;

                Owner  owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};
                auto&& subject = forwardOwner<SubjectSpec>(owner);

                // Classic 'if-else' access
                if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                {
                    if (subject)
                    [[maybe_unused]] SubjectElement& view = *subject;
                }
                else if constexpr (Spec::IsLockable<SubjectSpec>)
                {
                    if (auto subject_loc = subject.lock())
                    [[maybe_unused]] SubjectElement& view = *subject_loc;
                }

                // Unified 'if-else' access
                if (auto subject_loc = gain(subject))
                [[maybe_unused]] SubjectElement& view = *subject_loc;

                // Lambda access
                access(subject, [&](auto& subject)
                        {
                            [[maybe_unused]] SubjectElement& view = subject;
                        });

                // 'Range-based for' access
                for (auto& subject : subject)
                {
                    [[maybe_unused]] SubjectElement& view = *subject;
                }
            }
        }
    #endif
    }
};

template <class SubjectSpec>
void accessConstIncorrect()
{
    using SpecPair = std::pair<SubjectSpec, SubjectSpec>;

    using namespace Combo::Binary;
    Apply<AccessConstIncorrect, ConstIncorrectSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct AccessIncompatible
{
    template <class ... Tokens>
    static auto access(Tokens&& ... tokens)
    {
        return Helper::Access<typename SubjectSpec::Basis>::access(
            std::forward<Tokens>(tokens)...);
    }

    template <class Token>
    [[nodiscard]] static
    decltype(auto) gain(Token && token)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Token>(token));
    }

    template <class ... Tokens>
    [[nodiscard]] static
    auto gain(Tokens&& ... tokens)
    {
        return Helper::Gain<typename SubjectSpec::Basis>::gain(
            std::forward<Tokens>(tokens)...);
    }

    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectElement  = typename SubjectSpec::Element;
        using SourceElement   = typename SourceSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<SubjectElement>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;

        using Subject = Instance<SubjectSpec>;
        using Owner   = Instance<OwnerSpec>;

        INFO("Access is incompatible"
             "\nthe " + Utility::typeName<Subject>()
             + "{" + Utility::typeName<SubjectElement*>() + "}")
        {
            // INFO("Access the " + Utility::typeName<Subject>()
            //        + "\n      with " + Utility::typeName<SourceElement>())
            {
                Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};

                auto&& subject = forwardOwner<SubjectSpec>(owner);

                // Classic 'if-else' access
                if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                {
                    if (subject)
                    [[maybe_unused]] SourceElement& view = *subject;
                }
                else if constexpr (Spec::IsLockable<SubjectSpec>)
                {
                    if (auto subject_loc = subject.lock())
                    [[maybe_unused]] SourceElement& view = *subject_loc;
                }

                // Unified 'if-else' access
                if (auto subject_loc = gain(subject))
                [[maybe_unused]] SourceElement& view = *subject_loc;

                // Lambda access
                access(subject, [&](auto& subject)
                        {
                            [[maybe_unused]] SourceElement& view = subject;
                        });

                // 'Range-based for' access
                for (auto& subject : subject)
                {
                    [[maybe_unused]] SourceElement& view = *subject;
                }
            }

        #ifdef ENABLE_COMPILE_ERROR
            if constexpr (IsDerived<ConcreteElement, SubjectElement>)
            {
                // INFO("View of    base " + Utility::typeName<Subject>()
                //        + "\n   with derived " + Utility::typeName<ConcreteElement>())
                {
                    Owner owner{makeOwner<Owner, ConcreteElement>(literalOf<ConcreteElement>())};

                    auto&& subject = forwardOwner<SubjectSpec>(owner);

                    // Classic 'if-else' access
                    if constexpr (Spec::IsDereferenceable<SubjectSpec>)
                    {
                        if (subject)
                        [[maybe_unused]] const ConcreteElement& view = *subject;
                    }
                    else if constexpr (Spec::IsLockable<SubjectSpec>)
                    {
                        if (auto subject_loc = subject.lock())
                        [[maybe_unused]] const ConcreteElement& view = *subject_loc;
                    }

                    // Unified 'if-else' access
                    if (auto subject_loc = gain(subject))
                    [[maybe_unused]] const ConcreteElement& view = *subject_loc;

                    // Lambda access
                    access(subject, [&](auto& subject)
                            {
                                [[maybe_unused]] const ConcreteElement& view = subject;
                            });

                    // 'Range-based for' access
                    for (auto& subject : subject)
                    {
                        [[maybe_unused]] const ConcreteElement& view = *subject;
                    }
                }
            }

        #endif
        }
    #endif
    }
};

template <class SubjectSpec>
void accessIncompatible()
{
    using namespace Combo::Binary;
    Apply<AccessIncompatible, Combo::Unary::IncompatibleSet<SubjectSpec>>()();
}

} // namespace

template <class SubjectSpec>
struct AccessHelper<SubjectSpec>
{
    void operator()()
    {
        accessCompatible<SubjectSpec>();
        accessSeveral<SubjectSpec>();
        accessConstIncorrect<SubjectSpec>();
        accessIncompatible<SubjectSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
