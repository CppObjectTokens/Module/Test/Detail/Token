/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Utility.h>

#include <catch2/catch.hpp>

namespace Test
{

template <class Subject, class Raw>
std::enable_if_t<!Token::IsToken<Subject>, bool>
isPointTo(const Subject& subject, const Raw* raw)
{
    return std::addressof(subject) == raw;
}

template <class Subject>
std::enable_if_t<Token::IsToken<Subject>, bool>
isPointTo(const Subject& subject, const Trait::Element<Subject>* raw)
{
    return toAddress(subject) == raw;
}

template <class Subject>
bool isEmpty(const Subject& subject) noexcept
{
    bool result = true;

    if constexpr (  Token::IsDirectAccessible<Subject>
                 && Token::IsOptional<Subject>)
        result &= !subject;

    // TODO: Test Layout.
    if constexpr (!Token::IsMoveRelocateObject<Subject, Subject>)
        result &= isPointTo(subject, nullptr);

    return result;
}

template <class Subject>
std::enable_if_t<Token::IsDirectAccessible<Subject>, bool>
isNotEmpty(const Subject& subject) noexcept
{
    return toAddress(subject) != nullptr;
}

template <class Subject>
std::enable_if_t<Token::IsTrackableObserver<Subject>, bool>
isNotEmpty([[maybe_unused]] const Subject& subject) noexcept
{
    return false;
}

template <class Subject>
bool isMoved(const Subject& subject)
{
    bool result = true;

    if constexpr (Token::IsSingle<Subject>&& Token::IsObserver<Subject>)
    {
        if constexpr (Token::IsTrackable<Subject>)
            result &= subject.expired();
        else // Token::IsSingleUntrackableObserver
            result &= isNotEmpty(subject);
    }
    else // !Token::IsSingleObserver
    {
        result &= isEmpty(subject);
    }

    return result;
}

template <class SubjectSpec, class SourceSpec, class Source, class Raw>
bool isMoved(const Source& source, Raw* raw)
{
    bool result = true;

    if constexpr (Spec::IsMoveEmpty<SubjectSpec, SourceSpec>)
        result &= isMoved(source);
    else if constexpr (Spec::IsMoveUnspecified<SubjectSpec, SourceSpec>)
        result &= isPointTo(source, raw);

    return result;
}

template <class SubjectSpec, class SourceSpec, class Subject, class SourceRaw>
bool isObserverIntegrity(const Subject& subject, SourceRaw* source_raw)
{
    bool result = true;

    if constexpr (Spec::IsMoveInvalidateObservers<SubjectSpec, SourceSpec>)
        result &= !isPointTo(subject, source_raw);
    else
        result &= isPointTo(subject, source_raw);

    return result;
}

template <class Subject, class Literal>
std::enable_if_t<Token::IsOwner<Subject>, bool>
isEqual(const Subject& subject, const Literal& literal)
{
    bool result = true;

    if constexpr (Token::IsOptional<Subject>)
        result &= static_cast<bool>(subject);

    result &= toValue(subject) == literal;

    return result;
}

template <class Subject, class Literal>
std::enable_if_t<Token::IsObserver<Subject>, bool>
isEqual(const Subject& subject, const Literal& literal)
{
    bool result = !subject.expired();

    if constexpr (Token::IsDereferenceable<Subject>)
        result &= *subject == literal;
    else if constexpr (Token::IsLockable<Subject>)
        result &= *subject.lock() == literal;
    else
        result = false;

    return result;
}

} // namespace Test
