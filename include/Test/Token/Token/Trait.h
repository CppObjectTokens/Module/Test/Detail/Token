/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Spec/Trait.h>
#include <Test/Token/Trait.h>

namespace Test
{

namespace Token
{

// T - Token

// ----- Basis -----------------------------------------------------------------
template <class T>
inline constexpr bool IsUnknown = Test::Spec::IsUnknown<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsStdSmart = Test::Spec::IsStdSmart<Trait::Spec<T>>;
// ----- Basis -----------------------------------------------------------------

// ----- Ownership -------------------------------------------------------------
template <class T>
inline constexpr bool IsUnified = Test::Spec::IsUnified<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsUnique = Test::Spec::IsUnique<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsShared = Test::Spec::IsShared<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsWeak = Test::Spec::IsWeak<Trait::Spec<T>>;
// ----- Ownership -------------------------------------------------------------

// ----- Multiplicity ----------------------------------------------------------
template <class T>
inline constexpr bool IsMultiplicityUnknown = Test::Spec::IsMultiplicityUnknown<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsOptional = Test::Spec::IsOptional<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsSingle = Test::Spec::IsSingle<Trait::Spec<T>>;
// ----- Multiplicity ----------------------------------------------------------

// ----- Lifetime --------------------------------------------------------------
template <class T>
inline constexpr bool IsToken = Test::Spec::IsToken<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsOwner = Test::Spec::IsOwner<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsObserver = Test::Spec::IsObserver<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsTrackable = Test::Spec::IsTrackable<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsLockable = Test::Spec::IsLockable<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsExpirable = Test::Spec::IsExpirable<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsUntrackableObserver = Test::Spec::IsUntrackableObserver<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsTrackableObserver = Test::Spec::IsTrackableObserver<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsUntrackableOwner = Test::Spec::IsUntrackableOwner<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsTrackableOwner = Test::Spec::IsTrackableOwner<Trait::Spec<T>>;
// ----- Lifetime --------------------------------------------------------------

// ----- Access ----------------------------------------------------------------
template <class T>
inline constexpr bool IsDereferenceable = Test::Spec::IsDereferenceable<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsDirectAccessible = Test::Spec::IsDirectAccessible<Trait::Spec<T>>;

template <class T>
inline constexpr bool IsProxyAccessible = Test::Spec::IsProxyAccessible<Trait::Spec<T>>;
// ----- Access ----------------------------------------------------------------

// ----- Construction ----------------------------------------------------------
template <class Subject, class Source>
inline constexpr bool IsExplicitCopy =
    Test::Spec::IsExplicitCopy<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsExplicitMove =
    Test::Spec::IsExplicitMove<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsMoveEmpty =
    Test::Spec::IsMoveEmpty<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsMoveUnspecified =
    Test::Spec::IsMoveUnspecified<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsMoveRelocateObject =
    Test::Spec::IsMoveRelocateObject<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsMoveInvalidateObservers =
    Test::Spec::IsMoveInvalidateObservers<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsTolerateMovedFrom =
    Test::Spec::IsTolerateMovedFrom<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsElementCopyable =
    Test::Spec::IsElementCopyable<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsElementMoveConstructible =
    Test::Spec::IsElementMoveConstructible<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsElementMoveAssignable =
    Test::Spec::IsElementMoveAssignable<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsCopyNoexcept =
    Test::Spec::IsCopyNoexcept<Trait::Spec<Subject>, Trait::Spec<Source>>;

template <class Subject, class Source>
inline constexpr bool IsMoveNoexcept =
    Test::Spec::IsMoveNoexcept<Trait::Spec<Subject>, Trait::Spec<Source>>;
// ----- Construction ----------------------------------------------------------

// ----- Feature ---------------------------------------------------------------
// ----- Feature ---------------------------------------------------------------

} // namespace Token

} // namespace Test
