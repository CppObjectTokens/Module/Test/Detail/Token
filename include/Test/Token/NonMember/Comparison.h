/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Common.h>

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_BEGIN

#define ENABLE_COMPILE_ERROR_x

namespace Test
{

template <class SubjectSpec, class SourceSpec, class = void>
struct ComparisonNonMember
{
    static_assert(sizeof(SubjectSpec) == -1,
                  "Test specialization is defined for the SubjectSpec");
};

namespace
{

template <class SubjectSpec, class = void>
struct ComparisonWithNullptrValid
{
    void operator()()
    {
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;

        using Subject = Instance<SubjectSpec>;
        using Owner   = Instance<OwnerSpec>;

        auto literal_a = literalOf<ConcreteElement>();

        Owner    owner_a{makeOwner<Owner, ConcreteElement>(literal_a)};
        Element* raw_a = toAddress(owner_a);

        auto&& a = forwardOwner<SubjectSpec>(owner_a);

        bool a_less_nullptr = std::less<Element*>()(raw_a, nullptr);
        bool nullptr_less_a = std::less<Element*>()(nullptr, raw_a);

        INFO("Comparison is valid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nwith nullptr")
        {
            if constexpr (Spec::IsSingle<SubjectSpec>)
            {
            #ifndef OTN_DEPRECATE_SINGLE_BOOL
                REQUIRE((a == nullptr) == (raw_a == nullptr));
                REQUIRE((a != nullptr) == (raw_a != nullptr));
                O_T_CHECK_NOEXCEPT(a == nullptr);
            #endif
            }
            else
            {
                REQUIRE((a == nullptr) == (raw_a == nullptr));
                REQUIRE((a != nullptr) == (raw_a != nullptr));
                O_T_CHECK_NOEXCEPT(a == nullptr);
            }

        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            if constexpr (Detector::IsLessComparable<Subject, std::nullopt_t>)
            {
                REQUIRE((a < nullptr) == a_less_nullptr);
                REQUIRE((a > nullptr) == nullptr_less_a);
                REQUIRE((a <= nullptr) == !nullptr_less_a);
                REQUIRE((a >= nullptr) == !a_less_nullptr);

                O_T_CHECK_NOEXCEPT(a < nullptr);
            }

        #endif
        }

        INFO("Comparison is valid"
             "\n  of nullptr"
             "\nwith " + Utility::typeName<Subject>())
        {
            if constexpr (Spec::IsSingle<SubjectSpec>)
            {
            #ifndef OTN_DEPRECATE_SINGLE_BOOL
                REQUIRE((nullptr == a) == (nullptr == raw_a));
                REQUIRE((nullptr != a) == (nullptr != raw_a));
                O_T_CHECK_NOEXCEPT(nullptr == a);
            #endif
            }
            else
            {
                REQUIRE((nullptr == a) == (nullptr == raw_a));
                REQUIRE((nullptr != a) == (nullptr != raw_a));
                O_T_CHECK_NOEXCEPT(nullptr == a);
            }

        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            if constexpr (Detector::IsLessComparable<std::nullopt_t, Subject>)
            {
                REQUIRE((nullptr < a) == nullptr_less_a);
                REQUIRE((nullptr > a) == a_less_nullptr);
                REQUIRE((nullptr <= a) == !a_less_nullptr);
                REQUIRE((nullptr >= a) == !nullptr_less_a);

                O_T_CHECK_NOEXCEPT(nullptr < a);
            }

        #endif
        }

        cleanup<OwnerSpec, SubjectSpec>(raw_a);
    }
};

template <class SubjectSpec>
void comparisonWithNullptrValid()
{
    using namespace Combo::Unary;
    Apply<ComparisonWithNullptrValid, ConstSet<SubjectSpec>>()();
}

template <class SubjectSpec, class = void>
struct ComparisonWithNullptrInvalid
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using Element         = typename SubjectSpec::Element;
        using ConcreteElement = Conform::ConcreteOf<Element>;

        using OwnerSpec = Conform::OwnerSpec<SubjectSpec>;

        using Subject = Instance<SubjectSpec>;
        using Owner   = Instance<OwnerSpec>;

        auto literal_a = literalOf<ConcreteElement>();

        Owner    owner_a{makeOwner<Owner, ConcreteElement>(literal_a)};
        Element* raw_a = toAddress(owner_a);

        auto&& a = forwardOwner<SubjectSpec>(owner_a);

        [[maybe_unused]] bool a_less_nullptr = std::less<Element*>()(raw_a, nullptr);
        [[maybe_unused]] bool nullptr_less_a = std::less<Element*>()(nullptr, raw_a);

        INFO("Comparison is valid"
             "\n  of " + Utility::typeName<Subject>()
             + "\nwith nullptr")
        {
            REQUIRE((a == nullptr) == (raw_a == nullptr));
            REQUIRE((a != nullptr) == (raw_a != nullptr));
            REQUIRE((a < nullptr) == a_less_nullptr);
            REQUIRE((a > nullptr) == nullptr_less_a);
            REQUIRE((a <= nullptr) == !nullptr_less_a);
            REQUIRE((a >= nullptr) == !a_less_nullptr);
        }

        INFO("Comparison is valid"
             "\n  of nullptr"
             "\nwith " + Utility::typeName<Subject>())
        {
            REQUIRE((nullptr == a) == (nullptr == raw_a));
            REQUIRE((nullptr != a) == (nullptr != raw_a));
            REQUIRE((nullptr < a) == nullptr_less_a);
            REQUIRE((nullptr > a) == a_less_nullptr);
            REQUIRE((nullptr <= a) == !a_less_nullptr);
            REQUIRE((nullptr >= a) == !nullptr_less_a);
        }

        cleanup<OwnerSpec, SubjectSpec>(raw_a);
    #endif
    }
};

template <class SubjectSpec>
void comparisonWithNullptrInvalid()
{
    using namespace Combo::Unary;
    Apply<ComparisonWithNullptrInvalid, ConstSet<SubjectSpec>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ComparisonCompatible {};

template <class SubjectSpec, class SourceSpec>
struct ComparisonCompatible<SubjectSpec, SourceSpec,
                            std::enable_if_t<!(  Spec::IsStdSmart<SubjectSpec>
                                              && Spec::IsStdSmart<SourceSpec>)>>
{
    void operator()()
    {
        using ElementT         = typename SubjectSpec::Element;
        using ElementU         = typename SourceSpec::Element;
        using ConcreteElementT = Conform::ConcreteOf<ElementT>;
        using ConcreteElementU = Conform::ConcreteOf<ElementU>;

        using OwnerTSpec = Conform::OwnerSpec<SubjectSpec>;
        using OwnerUSpec = Conform::OwnerSpec<SourceSpec>;

        using SubjectT = Instance<SubjectSpec>;
        using SubjectU = Instance<SourceSpec>;
        using OwnerT   = Instance<OwnerTSpec>;
        using OwnerU   = Instance<OwnerUSpec>;

        INFO("Comparison is valid"
             "\n  of " + Utility::typeName<SubjectT>()
             + "\nwith " + Utility::typeName<SubjectU>())
        {
            auto literal_a = literalOf<ConcreteElementT>();
            auto literal_b = literalOf<ConcreteElementU>();

            OwnerT    owner_a{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
            OwnerU    owner_b{makeOwner<OwnerU, ConcreteElementU>(literal_b)};
            ElementT* raw_a = toAddress(owner_a);
            ElementU* raw_b = toAddress(owner_b);

            auto&& a = forwardOwner<SubjectSpec>(owner_a);
            auto&& b = forwardOwner<SourceSpec>(owner_b);

        #ifdef TOKEN_TEST_DETECTOR_ENABLED
            if constexpr (Detector::IsEqualityComparable<SubjectT, SubjectU>)
            {
                REQUIRE((a == b) == (raw_a == raw_b));
                REQUIRE((a != b) == (raw_a != raw_b));
                REQUIRE((a < b) == (raw_a < raw_b));
                REQUIRE((a > b) == (raw_a > raw_b));
                REQUIRE((a <= b) == (raw_a <= raw_b));
                REQUIRE((a >= b) == (raw_a >= raw_b));

                REQUIRE((b == a) == (raw_b == raw_a));
                REQUIRE((b != a) == (raw_b != raw_a));
                REQUIRE((b < a) == (raw_b < raw_a));
                REQUIRE((b > a) == (raw_b > raw_a));
                REQUIRE((b <= a) == (raw_b <= raw_a));
                REQUIRE((b >= a) == (raw_b >= raw_a));

                O_T_CHECK_NOEXCEPT(a == b);
                O_T_CHECK_NOEXCEPT(a < b);
                O_T_CHECK_NOEXCEPT(b < a);
            }

        #endif

            cleanup<OwnerTSpec, SubjectSpec>(raw_a);
            cleanup<OwnerUSpec, SourceSpec>( raw_b);
        }
    }
};

template <class SubjectSpec, class SourceSpec>
struct ComparisonCompatible<SubjectSpec, SourceSpec,
                            std::enable_if_t<(  Spec::IsStdSmart<SubjectSpec>
                                             && Spec::IsStdSmart<SourceSpec>)>>
{
    void operator()()
    {
        // Responsibility of the STL.
    }
};

template <class SubjectSpec, class SourceSpec>
void comparisonCompatible()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<ComparisonCompatible, ConstSet<SpecPair>>()();
}

template <class SubjectSpec, class SourceSpec, class = void>
struct ComparisonIncompatible
{
    void operator()()
    {
    #ifdef ENABLE_COMPILE_ERROR
        using SubjectT = Instance<SubjectSpec>;
        using SubjectU = Instance<SourceSpec>;

        INFO("Comparison is incompatible"
             "\n  of " + Utility::typeName<SubjectT>()
             + "\nwith " + Utility::typeName<SubjectU>())
        {
            using ElementT         = typename SubjectSpec::Element;
            using ElementU         = typename SourceSpec::Element;
            using ConcreteElementT = Conform::ConcreteOf<ElementT>;
            using ConcreteElementU = Conform::ConcreteOf<ElementU>;

            using OwnerT = Instance<Conform::OwnerSpec<SubjectSpec>>;
            using OwnerU = Instance<Conform::OwnerSpec<SourceSpec>>;

            auto literal_a = literalOf<ConcreteElementT>();
            auto literal_b = literalOf<ConcreteElementU>();

            OwnerT    owner_a{makeOwner<OwnerT, ConcreteElementT>(literal_a)};
            OwnerU    owner_b{makeOwner<OwnerU, ConcreteElementU>(literal_b)};
            ElementT* raw_a = toAddress(owner_a);
            ElementU* raw_b = toAddress(owner_b);

            auto&& a = forwardOwner<SubjectSpec>(owner_a);
            auto&& b = forwardOwner<SourceSpec>(owner_b);

            REQUIRE((a == b) == (raw_a == raw_b));
            REQUIRE((a != b) == (raw_a != raw_b));
            REQUIRE((a < b) == (raw_a < raw_b));
            REQUIRE((a > b) == (raw_a > raw_b));
            REQUIRE((a <= b) == (raw_a <= raw_b));
            REQUIRE((a >= b) == (raw_a >= raw_b));

            REQUIRE((b == a) == (raw_b == raw_a));
            REQUIRE((b != a) == (raw_b != raw_a));
            REQUIRE((b < a) == (raw_b < raw_a));
            REQUIRE((b > a) == (raw_b > raw_a));
            REQUIRE((b <= a) == (raw_b <= raw_a));
            REQUIRE((b >= a) == (raw_b >= raw_a));
        }
    #endif
    }
};

template <class SubjectSpec, class SourceSpec>
void comparisonIncompatible()
{
    using SpecPair = std::pair<SubjectSpec, SourceSpec>;

    using namespace Combo::Binary;
    Apply<ComparisonIncompatible, SpecPair>()();
}

} // namespace

template <class SubjectSpec, class SourceSpec>
struct ComparisonNonMember<SubjectSpec, SourceSpec,
                           std::enable_if_t<(  Spec::IsDereferenceable<SubjectSpec>
                                            && Spec::IsDereferenceable<SourceSpec>)>>
{
    void operator()()
    {
        comparisonWithNullptrValid<SubjectSpec>();

        comparisonCompatible<SubjectSpec, SourceSpec>();

        using SourceElement         = typename SourceSpec::Element;
        using ConcreteSourceElement = Conform::ConcreteOf<SourceElement>;
        using ConcreteSourceSpec    = Spec::ReplaceElement<SourceSpec, ConcreteSourceElement>;

        if constexpr (IsDerived<ConcreteSourceElement, SourceElement>)
            comparisonCompatible<SubjectSpec, ConcreteSourceSpec>();

        using IncompatibleElement = Conform::IncompatibleOf<SourceElement>;
        comparisonIncompatible<SubjectSpec, Spec::ReplaceElement<SourceSpec, IncompatibleElement>>();
    }
};

template <class SubjectSpec, class SourceSpec>
struct ComparisonNonMember<SubjectSpec, SourceSpec,
                           std::enable_if_t<!(  Spec::IsDereferenceable<SubjectSpec>
                                             && Spec::IsDereferenceable<SourceSpec>)>>
{
    void operator()()
    {
        comparisonWithNullptrInvalid<SubjectSpec>();
        comparisonIncompatible<SubjectSpec, SubjectSpec>();
    }
};

} // namespace Test

TOKEN_TEST_SUPPRESS_OTN_DEPRECATIONS_END
