/*
 * MIT License
 *
 * Copyright (c) 2018-2020 Viktor Kireev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <Test/Token/Spec/Trait.h>
#include <Test/Token/Instance.h>
#include <Test/Token/Conform.h>

#include <Test/Sample/Conform.h>

#include <tuple>

// *INDENT-OFF*
#define SPEC_UNARY_FUNCTOR \
template <class SubjectSpec, class = void> class Functor

#define SPEC_BINARY_FUNCTOR \
template <class SubjectSpec, class SourceSpec, class = void> class Functor
// *INDENT-ON*

namespace Test
{

namespace Combo
{

namespace Unary
{

template <SPEC_UNARY_FUNCTOR, class ... SpecTuples>
struct Apply
{
    void operator()()
    { (Apply<Functor, SpecTuples>()(), ...); }
};

template <SPEC_UNARY_FUNCTOR, class ... Specs>
struct Apply<Functor, std::tuple<Specs ...>>
{
    void operator()()
    { (Apply<Functor, Specs>()(), ...); }
};

template <SPEC_UNARY_FUNCTOR, class Spec>
struct Apply<Functor, Spec>
{
    void operator()()
    {
        if constexpr (!std::is_same_v<Instance<Spec>, void>)
            Functor<Spec>()();
    }
};

namespace Internal
{

template <template <class Spec, class Value> class SpecEditor,
          class Spec, class Values>
struct SpecSet {};

template <template <class Spec, class Value> class SpecEditor,
          class Spec, class ... Values>
struct SpecSet<SpecEditor, Spec, std::tuple<Values...>>
{ using Type = std::tuple<SpecEditor<Spec, Values>...>; };

template <class SubjectSpec>
struct ConstSet
{
    using Element = typename SubjectSpec::Element;

    using Type = std::tuple<SubjectSpec,
                            Spec::ReplaceElement<SubjectSpec, const Element>>;
};

template <class ... Specs>
struct ConstSet<std::tuple<Specs ...>>
{
    using Type =
        decltype(std::tuple_cat(typename ConstSet<Specs>::Type {} ...));
};

template <class SubjectSpec>
struct IncompatibleSet
{
    using Element    = typename SubjectSpec::Element;
    using SourceSpec = Spec::ReplaceElement<SubjectSpec,
                                            Conform::IncompatibleOf<Element>>;

    using Type = std::pair<SubjectSpec, SourceSpec>;
};

} // namespace Internal

template <class S, class ElementList>
using ElementSet =
    typename Internal::SpecSet<Spec::ReplaceElement, S, ElementList>::Type;

template <class S, class BasisList>
using BasisSet =
    typename Internal::SpecSet<Spec::ReplaceBasis, S, BasisList>::Type;

template <class S, class OwnershipList>
using OwnershipSet =
    typename Internal::SpecSet<Spec::ReplaceOwnership, S, OwnershipList>::Type;

template <class S, class MultiplicityList>
using MultiplicitySet =
    typename Internal::SpecSet<Spec::ReplaceMultiplicity, S, MultiplicityList>::Type;

template <class ... Specs>
using ConstSet =
    typename Internal::ConstSet<Specs ...>::Type;

template <class SubjectSpec>
using IncompatibleSet =
    typename Internal::IncompatibleSet<SubjectSpec>::Type;

template <SPEC_UNARY_FUNCTOR,
          class ElementSet,
          class SpecSet>
struct ApplySpecSets
{
    template <class Element, class Component, std::size_t ... I>
    void combineComponents(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t m_s = tuple_size_v<typename Component::Multiplicities>;
        constexpr size_t o_s = tuple_size_v<typename Component::Ownerships>;
        constexpr size_t b_s = tuple_size_v<typename Component::Bases>;

        // Component divisors.
        constexpr size_t m_d = 1;
        constexpr size_t o_d = m_d * m_s;
        constexpr size_t b_d = o_d * o_s;

        (Apply<
             Functor,
             Spec::Generic<Element,
                           tuple_element_t<I / b_d % b_s, typename Component::Bases>,
                           tuple_element_t<I / o_d % o_s, typename Component::Ownerships>,
                           tuple_element_t<I / m_d % m_s, typename Component::Multiplicities>>>{} (), ...);
    }

    template <class Element, std::size_t ... I>
    void combineSpecs(std::index_sequence<I...>)
    {
        using namespace std;

        (combineComponents<Element, tuple_element_t<I, SpecSet>>(
             make_index_sequence<tuple_size_v<typename tuple_element_t<I, SpecSet>::Bases>*
                                 tuple_size_v<typename tuple_element_t<I, SpecSet>::Ownerships>*
                                 tuple_size_v<typename tuple_element_t<I, SpecSet>::Multiplicities>>{}), ...);
    }

    template <std::size_t ... I>
    void combineElements(std::index_sequence<I...>)
    {
        using namespace std;

        (combineSpecs<tuple_element_t<I, ElementSet>>(
             make_index_sequence<tuple_size_v<SpecSet>>{}), ...);
    }

    void operator()()
    {
        using namespace std;

        combineElements(
            make_index_sequence<tuple_size_v<ElementSet>>{});
    }
};

template <SPEC_UNARY_FUNCTOR,
          class ElementList,
          class BasisList,
          class OwnershipList,
          class MultiplicityList>
struct ApplySpecLists
{
    template <std::size_t ... I>
    void combineSpecs(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t m_s = tuple_size_v<MultiplicityList>;
        constexpr size_t o_s = tuple_size_v<OwnershipList>;
        constexpr size_t b_s = tuple_size_v<BasisList>;
        constexpr size_t e_s = tuple_size_v<ElementList>;

        // Component divisors.
        constexpr size_t m_d = 1;
        constexpr size_t o_d = m_d * m_s;
        constexpr size_t b_d = o_d * o_s;
        constexpr size_t e_d = b_d * b_s;

        (Apply<
             Functor,
             Spec::Generic<tuple_element_t<I / e_d % e_s, ElementList>,
                           tuple_element_t<I / b_d % b_s, BasisList>,
                           tuple_element_t<I / o_d % o_s, OwnershipList>,
                           tuple_element_t<I / m_d % m_s, MultiplicityList>>>{} (), ...);
    }

    void operator()()
    {
        using namespace std;

        combineSpecs(
            make_index_sequence<tuple_size_v<ElementList>*
                                tuple_size_v<BasisList>*
                                tuple_size_v<OwnershipList>*
                                tuple_size_v<MultiplicityList>>{});
    }
};

} // namespace Unary

namespace Binary
{

template <SPEC_BINARY_FUNCTOR, class ... SpecTuples>
struct Apply
{
    void operator()()
    { (Apply<Functor, SpecTuples>()(), ...); }
};

template <SPEC_BINARY_FUNCTOR, class ... SpecTuples>
struct Apply<Functor, std::tuple<SpecTuples ...>>
{
    void operator()()
    { (Apply<Functor, SpecTuples>()(), ...); }
};

template <SPEC_BINARY_FUNCTOR, class SubjectSpec, class SourceSpec>
struct Apply<Functor, std::pair<SubjectSpec, SourceSpec>>
{
    void operator()()
    {
        if constexpr (!(  std::is_same_v<Instance<SubjectSpec>, void>
                       || std::is_same_v<Instance<SourceSpec>, void>))
            Functor<SubjectSpec, SourceSpec>()();
    }
};

namespace Internal
{

template <class Subject>
struct ConstSet
{
    static_assert(sizeof(Subject) == -1,
                  "ConstSet specialization is defined "
                  "for the Subject");
};

template <class SubjectSpec, class SourceSpec>
struct ConstSet<std::pair<SubjectSpec, SourceSpec>>
{
    using SubjectElement = typename SubjectSpec::Element;
    using SourceElement  = typename SourceSpec::Element;

    using Type = std::tuple<
        std::pair<SubjectSpec, SourceSpec>,
        std::pair<Spec::ReplaceElement<SubjectSpec, const SubjectElement>,
                  Spec::ReplaceElement<SourceSpec, const SourceElement>>,
        std::pair<Spec::ReplaceElement<SubjectSpec, const SubjectElement>, SourceSpec>,
        std::pair<SubjectSpec, Spec::ReplaceElement<SourceSpec, const SourceElement>>>;
};

template <class ... SpecPairs>
struct ConstSet<std::tuple<SpecPairs ...>>
{
    using Type =
        decltype(std::tuple_cat(typename ConstSet<SpecPairs>::Type {} ...));
};

template <class Subject>
struct ConstCorrectSet
{
    static_assert(sizeof(Subject) == -1,
                  "ConstCorrectSet specialization is defined "
                  "for the Subject");
};

template <class SubjectSpec, class SourceSpec>
struct ConstCorrectSet<std::pair<SubjectSpec, SourceSpec>>
{
    using Element = typename SubjectSpec::Element;

    using Type = std::tuple<
        std::pair<SubjectSpec, SourceSpec>,
        std::pair<Spec::ReplaceElement<SubjectSpec, const Element>,
                  Spec::ReplaceElement<SourceSpec, const Element>>,
        std::pair<Spec::ReplaceElement<SubjectSpec, const Element>, SourceSpec>>;
};

template <class ... SpecPairs>
struct ConstCorrectSet<std::tuple<SpecPairs ...>>
{
    using Type =
        decltype(std::tuple_cat(typename ConstCorrectSet<SpecPairs>::Type {} ...));
};

template <class Subject>
struct ConstIncorrectSet
{
    static_assert(sizeof(Subject) == -1,
                  "ConstIncorrectSet specialization is defined "
                  "for the Subject");
};

template <class SubjectSpec, class SourceSpec>
struct ConstIncorrectSet<std::pair<SubjectSpec, SourceSpec>>
{
    using Element = typename SubjectSpec::Element;

    using Type = std::tuple<
        std::pair<SubjectSpec, Spec::ReplaceElement<SourceSpec, const Element>>>;
};

template <class ... SpecPairs>
struct ConstIncorrectSet<std::tuple<SpecPairs ...>>
{
    using Type =
        decltype(std::tuple_cat(typename ConstIncorrectSet<SpecPairs>::Type {} ...));
};

template <class SubjectSpec, class SourceSpec>
struct IncompatibleSet
{
    using Element = typename SourceSpec::Element;
    using IncompatibleSpec =
        Spec::ReplaceElement<SourceSpec, Conform::IncompatibleOf<Element>>;

    using Type = std::pair<SubjectSpec, IncompatibleSpec>;
};

} // namespace Internal

template <class ... SpecPairs>
using ConstSet =
    typename Internal::ConstSet<SpecPairs ...>::Type;

template <class ... SpecPairs>
using ConstCorrectSet =
    typename Internal::ConstCorrectSet<SpecPairs ...>::Type;

template <class ... SpecPairs>
using ConstIncorrectSet =
    typename Internal::ConstIncorrectSet<SpecPairs ...>::Type;

template <class SubjectSpec, class SourceSpec>
using IncompatibleSet =
    typename Internal::IncompatibleSet<SubjectSpec, SourceSpec>::Type;

template <SPEC_BINARY_FUNCTOR, class SubjectSpec, class SourceSpec>
void applySpecs()
{
    if constexpr (!std::is_same_v<Instance<SourceSpec>, void>)
        Functor<SubjectSpec, SourceSpec>()();
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class Component,
          std::size_t ... I>
void combineSourceComponents(std::index_sequence<I...>)
{
    using namespace std;

    // Component sizes.
    constexpr size_t m_s = tuple_size_v<typename Component::Multiplicities>;
    constexpr size_t o_s = tuple_size_v<typename Component::Ownerships>;
    constexpr size_t b_s = tuple_size_v<typename Component::Bases>;

    // Component divisors.
    constexpr size_t m_d = 1;
    constexpr size_t o_d = m_d * m_s;
    constexpr size_t b_d = o_d * o_s;

    (applySpecs<
         Functor,
         SubjectSpec,
         Spec::Generic<SourceElement,
                       tuple_element_t<I / b_d % b_s, typename Component::Bases>,
                       tuple_element_t<I / o_d % o_s, typename Component::Ownerships>,
                       tuple_element_t<I / m_d % m_s, typename Component::Multiplicities>>>(), ...);
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceSpecSet,
          std::size_t ... I>
void combineSourceSpecs(std::index_sequence<I...>)
{
    using namespace std;

    (combineSourceComponents<Functor,
                             SubjectSpec,
                             SourceElement,
                             tuple_element_t<I, SourceSpecSet>>(
         make_index_sequence<tuple_size_v<typename tuple_element_t<I, SourceSpecSet>::Bases>*
                             tuple_size_v<typename tuple_element_t<I, SourceSpecSet>::Ownerships>*
                             tuple_size_v<typename tuple_element_t<I, SourceSpecSet>::Multiplicities>>{}), ...);
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceSpecSet>
std::enable_if_t<!std::is_same_v<Instance<SubjectSpec>, void>>
combineSubjectSpec()
{
    using namespace std;

    combineSourceSpecs<Functor,
                       SubjectSpec,
                       SourceElement,
                       SourceSpecSet>(
        make_index_sequence<tuple_size_v<SourceSpecSet>>{});
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceSpecSet>
std::enable_if_t<std::is_same_v<Instance<SubjectSpec>, void>>
combineSubjectSpec() {}

template <SPEC_BINARY_FUNCTOR,
          class SubjectElementSet,
          class SubjectSpecSet,
          class SourceElementSet,
          class SourceSpecSet>
struct ApplySpecSets
{
    template <class SubjectElement, class SourceElement, class Component, std::size_t ... I>
    void combineSubjectComponents(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t m_s = tuple_size_v<typename Component::Multiplicities>;
        constexpr size_t o_s = tuple_size_v<typename Component::Ownerships>;
        constexpr size_t b_s = tuple_size_v<typename Component::Bases>;

        // Component divisors.
        constexpr size_t m_d = 1;
        constexpr size_t o_d = m_d * m_s;
        constexpr size_t b_d = o_d * o_s;

        (combineSubjectSpec<
             Functor,
             Spec::Generic<SubjectElement,
                           tuple_element_t<I / b_d % b_s, typename Component::Bases>,
                           tuple_element_t<I / o_d % o_s, typename Component::Ownerships>,
                           tuple_element_t<I / m_d % m_s, typename Component::Multiplicities>>,
             SourceElement,
             SourceSpecSet>(), ...);
    }

    template <class SubjectElement, class SourceElement, std::size_t ... I>
    void combineSubjectSpecs(std::index_sequence<I...>)
    {
        using namespace std;

        (combineSubjectComponents<SubjectElement, SourceElement, tuple_element_t<I, SubjectSpecSet>>(
             make_index_sequence<tuple_size_v<typename tuple_element_t<I, SubjectSpecSet>::Bases>*
                                 tuple_size_v<typename tuple_element_t<I, SubjectSpecSet>::Ownerships>*
                                 tuple_size_v<typename tuple_element_t<I, SubjectSpecSet>::Multiplicities>>{}), ...);
    }

    template <class SubjectElement, class SourceElement>
    void combineCompatibleElements()
    {
        using namespace std;

        if constexpr (  std::is_same_v<SubjectElement, SourceElement>
                     || std::is_base_of_v<SubjectElement, SourceElement>)
            combineSubjectSpecs<SubjectElement, SourceElement>(
                make_index_sequence<tuple_size_v<SubjectSpecSet>>{});
    }

    template <std::size_t ... I>
    void combineElements(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t srs_s = tuple_size_v<SourceElementSet>;
        constexpr size_t sbj_s = tuple_size_v<SubjectElementSet>;

        // Component divisors.
        constexpr size_t srs_d = 1;
        constexpr size_t sbj_d = srs_d * srs_s;

        (combineCompatibleElements<tuple_element_t<I / sbj_d % sbj_s, SubjectElementSet>,
                                   tuple_element_t<I / srs_d % srs_s, SourceElementSet>>(), ...);
    }

    void operator()()
    {
        using namespace std;

        combineElements(
            make_index_sequence<tuple_size_v<SubjectElementSet>*
                                tuple_size_v<SourceElementSet>>{});
    }
};

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceBasisList,
          class SourceOwnershipList,
          class SourceMultiplicityList,
          std::size_t ... I>
void combineSourceSpecs(std::index_sequence<I...>)
{
    using namespace std;

    // Component sizes.
    constexpr size_t m_s = tuple_size_v<SourceMultiplicityList>;
    constexpr size_t o_s = tuple_size_v<SourceOwnershipList>;
    constexpr size_t b_s = tuple_size_v<SourceBasisList>;

    // Component divisors.
    constexpr size_t m_d = 1;
    constexpr size_t o_d = m_d * m_s;
    constexpr size_t b_d = o_d * o_s;

    (applySpecs
     <Functor,
      SubjectSpec,
      Spec::Generic<SourceElement,
                    tuple_element_t<I / b_d % b_s, SourceBasisList>,
                    tuple_element_t<I / o_d % o_s, SourceOwnershipList>,
                    tuple_element_t<I / m_d % m_s, SourceMultiplicityList>>>(), ...);
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceBasisList,
          class SourceOwnershipList,
          class SourceMultiplicityList>
std::enable_if_t<!std::is_same_v<Instance<SubjectSpec>, void>>
combineSubjectSpec()
{
    using namespace std;

    combineSourceSpecs<Functor,
                       SubjectSpec,
                       SourceElement,
                       SourceBasisList,
                       SourceOwnershipList,
                       SourceMultiplicityList>(
        make_index_sequence<tuple_size_v<SourceBasisList>*
                            tuple_size_v<SourceOwnershipList>*
                            tuple_size_v<SourceMultiplicityList>>{});
}

template <SPEC_BINARY_FUNCTOR,
          class SubjectSpec,
          class SourceElement,
          class SourceBasisList,
          class SourceOwnershipList,
          class SourceMultiplicityList>
std::enable_if_t<std::is_same_v<Instance<SubjectSpec>, void>>
combineSubjectSpec() {}

template <SPEC_BINARY_FUNCTOR,
          class SubjectElementList,
          class SubjectBasisList,
          class SubjectOwnershipList,
          class SubjectMultiplicityList,
          class SourceElementList,
          class SourceBasisList,
          class SourceOwnershipList,
          class SourceMultiplicityList>
struct ApplySpecLists
{
    template <class SubjectElement, class SourceElement, std::size_t ... I>
    void combineSubjectSpecs(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t m_s = tuple_size_v<SubjectMultiplicityList>;
        constexpr size_t o_s = tuple_size_v<SubjectOwnershipList>;
        constexpr size_t b_s = tuple_size_v<SubjectBasisList>;

        // Component divisors.
        constexpr size_t m_d = 1;
        constexpr size_t o_d = m_d * m_s;
        constexpr size_t b_d = o_d * o_s;

        (combineSubjectSpec
         <Functor,
          Spec::Generic<SubjectElement,
                        tuple_element_t<I / b_d % b_s, SubjectBasisList>,
                        tuple_element_t<I / o_d % o_s, SubjectOwnershipList>,
                        tuple_element_t<I / m_d % m_s, SubjectMultiplicityList>>,
          SourceElement,
          SourceBasisList,
          SourceOwnershipList,
          SourceMultiplicityList>(), ...);
    }

    template <class SubjectElement, class SourceElement>
    void combineCompatibleElements()
    {
        using namespace std;

        if constexpr (  std::is_same_v<SubjectElement, SourceElement>
                     || std::is_base_of_v<SubjectElement, SourceElement>)
            combineSubjectSpecs<SubjectElement, SourceElement>(
                make_index_sequence<tuple_size_v<SubjectBasisList>*
                                    tuple_size_v<SubjectOwnershipList>*
                                    tuple_size_v<SubjectMultiplicityList>>{});
    }

    template <std::size_t ... I>
    void combineElements(std::index_sequence<I...>)
    {
        using namespace std;

        // Component sizes.
        constexpr size_t srs_s = tuple_size_v<SourceElementList>;
        constexpr size_t sbj_s = tuple_size_v<SubjectElementList>;

        // Component divisors.
        constexpr size_t srs_d = 1;
        constexpr size_t sbj_d = srs_d * srs_s;

        (combineCompatibleElements<tuple_element_t<I / sbj_d % sbj_s, SubjectElementList>,
                                   tuple_element_t<I / srs_d % srs_s, SourceElementList>>(), ...);
    }

    void operator()()
    {
        using namespace std;

        combineElements(
            make_index_sequence<tuple_size_v<SubjectElementList>*
                                tuple_size_v<SourceElementList>>{});
    }
};

} // namespace Binary

} // namespace Combo

} // namespace Test
